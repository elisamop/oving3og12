var mysql = require("mysql");

const NyhetsDao = require("../../src/dao/nyhetsdao.js");
const runsqlfile = require("../../src/dao/runsqlfile.js");

//Kobler opp databasen //Dette må flyttes ut etterpå for dette ønsker man ikke i kildekoden
var pool = mysql.createPool({
  connectionLimit: 1,
  host: "mysql.stud.iie.ntnu.no",
  user: "elisamop",
  password: "ktWkDPy1",
  database: "elisamop",
  debug: false,
  multipleStatements: true
});

let nyhetsDao = new NyhetsDao(pool);

//Metoder for å starte og avslutte pool
beforeAll(done => {
  runsqlfile("src/dao/create_table.sql", pool, () => {
    runsqlfile("src/dao/insert_table.sql", pool, done);
  });
});

afterAll(() => {
  pool.end();
});

//TESTENE://

//Hent alle nyheter:
test("hent alle nyheter fra db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data.length=" + data.length
    );
    expect(data.length).toBeGreaterThanOrEqual(6);
    done();
  }

  nyhetsDao.getAll(callback);
});

//Hent alle nyheter for en spesifik kategori:
test("henter alle nyheter i kategori fra db", done => {
  function callback(status, data){
    console.log("Test callback: status=" + status + ", data.length=" + data.length
  );
  expect(data.length).toBeGreaterThanOrEqual(1);
  done();
  }

  nyhetsDao.getKategoryNews("sport", callback);
})

//Hent en spesifik nyhet:
test("henter en nyhet fra db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(1);
    expect(data[0].overskrift).toBe("Tropisk aroma-kake");
    done();
  }

  nyhetsDao.getOne(1, callback);
});


test("henter en ugyldig nyhet fra db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(0);
    done();
  }

  nyhetsDao.getOne(0, callback);
});

//Legg til en ny nyhet:
test("legger til ny nyhet i db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    console.log("Antall rader effected" + data.affectedRows);
    expect(data.affectedRows).toBeGreaterThanOrEqual(1);
    done();
  }
  nyhetsDao.createOne(
    {overskrift: "Skirabalder",
    innhold: "Det var mye rabalder under skiVM i dag",
    bildeurl: "https://smp.vgc.no/v2/images/53077f05-4966-4dd4-bc64-b36b077e0d51?fit=crop&h=1240&w=1900&s=11607c798a04e15d79a38565073a8c3dcc19ef1c",
    bildetekst:"skirabalder",
    kategori_id:1,
    viktighet:1},
    callback
  );
});


//Hent nyhet til livefeed: (DENNE BRUKES IKKE)
test("henter nyheter til livefeeden", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    //expect(data[0].id).toBe(7);
    expect(data.length).toBe(1);
    done();
  }

  nyhetsDao.getLivefeedNews(callback);
});


//Oppdater en nyhet:
test("Oppdater en nyhet i db", done => {
  function callback(status, data) {
    console.log(
      "Test callvack: status=" + status + ", data=" + JSON.stringify(data) + ", data.length=" + data.length
    );
    expect(data[0].overskrift).toBe("Tullball");
    done();
  }

  nyhetsDao.updateOne(
      {overskrift: "Tullball",
      innhold: "Det var mye tull på Karmøy i dag",
      bildeurl: "https://smp.vgc.no/v2/images/53077f05-4966-4dd4-bc64-b36b077e0d51?fit=crop&h=1240&w=1900&s=11607c798a04e15d79a38565073a8c3dcc19ef1c",
      bildetekst:"tullball",
      kategori_id:3,
      viktighet:2,
      id: 6},
      (status, data) => {
        nyhetsDao.getOne(6, callback)
  });
});


//Slett en nyhet:
test("slett nyhet fra db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data.length=" + data.length
    );
    expect(data.length).toBe(0);
    done();
  }

  nyhetsDao.deleteOne(7, (status, data) => {
    nyhetsDao.getOne(7, callback)
  });
});
