// @flow

const Dao = require('./dao.js');

class Nyhet {
id: number;
overskrift: string;
innhold: string;
bildeurl: string;
bildetekst: string;
kategori_id: number;
viktighet: number;

}

class Kategori {
  id: number;
  kategorinavn: string;
}

class SqlResult {}

module.exports = class NyhetsDao extends Dao {
  getAll(callback: (Nyhet[]) => mixed) {
    super.query('SELECT id, overskrift, innhold, bildeurl, bildetekst, kategori_id, viktighet, DATE_FORMAT(tidspunkt, "%Y-%m-%d kl: %H:%i") AS tidspunkt FROM nyheter ORDER BY viktighet, tidspunkt DESC;',
    [], callback);
  }

  getKategoryNews(kategorinavn : string, callback : Kategori[] => mixed) {
    super.query(
      'SELECT n.id, n.overskrift, n.innhold, n.bildeurl, n.bildetekst, n.kategori_id, n.viktighet, DATE_FORMAT(n.tidspunkt, "%Y-%m-%d kl: %H:%i") AS tidspunkt FROM nyheter n INNER JOIN kategorier ON n.kategori_id = kategorier.id WHERE kategorinavn = ? ORDER BY viktighet, tidspunkt DESC',
      [kategorinavn],
      callback
    );
  }

  getOne(id : number, callback : Nyhet => mixed) {
    super.query('SELECT id, overskrift, innhold, bildeurl, bildetekst, kategori_id, viktighet, DATE_FORMAT(tidspunkt, "%Y-%m-%d kl: %H:%i") AS tidspunkt FROM nyheter WHERE id=?',
    [id], callback);
  }

  //DENNE BRUKES IKKE
  getLivefeedNews(callback : Nyhet[] => mixed) {
    super.query(
      'SELECT id, overskrift, innhold, bildeurl, bildetekst, kategori_id, viktighet, DATE_FORMAT(tidspunkt, "%Y-%m-%d kl: %H:%i") AS tidspunkt FROM nyheter WHERE (DATE(tidspunkt) = CURRENT_DATE()) && viktighet = 1 ORDER BY tidspunkt',
      //Legg til (SELECT TIMEDIFF(DATE(tidspunkt), CURRENTDATE())) <= 1
      [], callback);
  }

  createOne(json : any, callback : Nyhet => mixed) {
    //var tidspunktN = new Date().toISOString().slice(0, 19).replace('T', ' '); //Gjør om fra JS date til mysql datetime
    var val = [json.overskrift, json.innhold, json.bildeurl, json.bildetekst, json.kategori_id, json.viktighet];

    super.query(
      'INSERT INTO nyheter (overskrift,innhold,tidspunkt,bildeurl,bildetekst,kategori_id,viktighet) VALUES (?,?,NOW(),?,?,?,?)',
      val,
      callback
    );
  }

  updateOne(json : any, callback : Nyhet => mixed) {
    var val = [
      json.overskrift,
      json.innhold,
      json.bildeurl,
      json.bildetekst,
      json.kategori_id,
      json.viktighet,
      json.id
    ];
    //var tidspunkt = new Date().toISOString().slice(0, 19).replace('T', ' '); //Gjør om fra JS date til mysql datetime
    super.query(
      'UPDATE nyheter SET overskrift=?, innhold=?, bildeurl=?, bildetekst=?, kategori_id=?, viktighet=? WHERE id=?',
      val,
      callback
    );
  }

  deleteOne(id : number, callback : Nyhet => mixed) {
    super.query('DELETE FROM nyheter WHERE id = ?', [id], callback);
  }
};
