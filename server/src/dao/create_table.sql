DROP TABLE nyheter;
DROP TABLE kategorier;

CREATE TABLE nyheter (
  id INTEGER NOT NULL AUTO_INCREMENT,
  overskrift VARCHAR(200) NOT NULL,
  innhold VARCHAR(20000) NOT NULL,
  tidspunkt DATETIME,
  bildeurl VARCHAR(500),
  bildetekst VARCHAR(1000),
  kategori_id INTEGER NOT NULL DEFAULT 1,
  viktighet INTEGER DEFAULT 1,
  CONSTRAINT nyheter_pk PRIMARY KEY(id)
);

CREATE TABLE kategorier (
  id INTEGER NOT NULL AUTO_INCREMENT,
  kategorinavn VARCHAR(50),
  CONSTRAINT kategorier_pk PRIMARY KEY(id)
);

ALTER TABLE nyheter
ADD CONSTRAINT nyheter_fk FOREIGN KEY(kategori_id)
REFERENCES kategorier(id);
