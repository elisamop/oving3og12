INSERT INTO kategorier (kategorinavn)
VALUES ('Sport'), ('Helse'), ('Kjendisnytt'), ('Miljø'), ('Samfunn');

INSERT INTO nyheter (overskrift, innhold, tidspunkt, bildeurl, bildetekst, kategori_id, viktighet)
VALUES
(
  'Tropisk aroma-kake',
  'Den marmorerte krydderkaken med sjokoladeglasur er en gammel slager som igjen har begynt å dukke opp på kakebordene. Den vekker minner for mange og finnes både i lyse og mørke varianter, som oftest i en kombinasjon.

Tropisk aroma lages gjerne i springform med et lag med sjokoladeglasur i midten. For å gjøre det litt enklere har jeg satt sammen en oppskrift som passer til en liten langpanne. Glasuren strykes på toppen og det klassiske mønsteret er laget ved hjelp av en gaffel. Kaken er saftig og holder seg i flere dager. Den er kjapp å lage og lettvint å ta med på sommeravslutningen!

Tips! Kryddermengden er beskjeden, men om du ikke er så begeistret, bytt ut kanel og muskat med 2 ts vaniljesukker. Da får du en saftig og god marmorkake, som slett ikke behøver sjokoladeglasur på toppen. Den marmorerte krydderkaken med sjokoladeglasur er en gammel slager som igjen har begynt å dukke opp på kakebordene. Den vekker minner for mange og finnes både i lyse og mørke varianter, som oftest i en kombinasjon.

Tropisk aroma lages gjerne i springform med et lag med sjokoladeglasur i midten. For å gjøre det litt enklere har jeg satt sammen en oppskrift som passer til en liten langpanne. Glasuren strykes på toppen og det klassiske mønsteret er laget ved hjelp av en gaffel. Kaken er saftig og holder seg i flere dager. Den er kjapp å lage og lettvint å ta med på sommeravslutningen!

Tips! Kryddermengden er beskjeden, men om du ikke er så begeistret, bytt ut kanel og muskat med 2 ts vaniljesukker. Da får du en saftig og god marmorkake, som slett ikke behøver sjokoladeglasur på toppen. Den marmorerte krydderkaken med sjokoladeglasur er en gammel slager som igjen har begynt å dukke opp på kakebordene. Den vekker minner for mange og finnes både i lyse og mørke varianter, som oftest i en kombinasjon.
Tips! Kryddermengden er beskjeden, men om du ikke er så begeistret, bytt ut kanel og muskat med 2 ts vaniljesukker. Da får du en saftig og god marmorkake, som slett ikke behøver sjokoladeglasur på toppen. ',
  '2018-11-18 12:44:33',
  'https://imbo.vgc.no/users/godt/images/26470bd9e656d092e4070bdd387ef805.jpg?t%5B0%5D=resize%3Awidth%3D1960&t%5B1%5D=strip&t%5B2%5D=compress%3Alevel%3D75&t%5B3%5D=progressive&accessToken=bb3a341aaf7c431d2e3b54c23c2f4d30460ecabd5754a5012be0535edf91388c',
  'Kake',
  2,
  2
),
(
  'Amanda Knox forlovet etter ellevilt frieri',
  'For elleve år siden ble 31-åringen arrestert og sendt i fengsel for det brutale drapet på sin engelske samboer Meredith Kercher (21) i Italia. Saken fikk enorm oppmerksomhet over hele verden, inkludert i Norge. Etter fire år i fengsel ble Knox blankt frikjent, og hun flyttet tilbake til Seattle, hvor hun i dag jobber som journalist og lever lykkelig med sin nå forlovede.

Frieriet var planlagt i ett år og Robinson har delt en video av frieriet i sosiale medier.

Videoen, som det kommende ekteparet har gitt VG tillatelse til å publisere, starter med at de to går ut i hagen etter å ha hørt et høyt smell og si-fi-musikk. Ute i hagen finner de det som ligner på en meteoritt med blått og lilla lys. For elleve år siden ble 31-åringen arrestert og sendt i fengsel for det brutale drapet på sin engelske samboer Meredith Kercher (21) i Italia. Saken fikk enorm oppmerksomhet over hele verden, inkludert i Norge. Etter fire år i fengsel ble Knox blankt frikjent, og hun flyttet tilbake til Seattle, hvor hun i dag jobber som journalist og lever lykkelig med sin nå forlovede.

Frieriet var planlagt i ett år og Robinson har delt en video av frieriet i sosiale medier.

Videoen, som det kommende ekteparet har gitt VG tillatelse til å publisere, starter med at de to går ut i hagen etter å ha hørt et høyt smell og si-fi-musikk. Ute i hagen finner de det som ligner på en meteoritt med blått og lilla lys. For elleve år siden ble 31-åringen arrestert og sendt i fengsel for det brutale drapet på sin engelske samboer Meredith Kercher (21) i Italia. Saken fikk enorm oppmerksomhet over hele verden, inkludert i Norge. Etter fire år i fengsel ble Knox blankt frikjent, og hun flyttet tilbake til Seattle, hvor hun i dag jobber som journalist og lever lykkelig med sin nå forlovede.

Frieriet var planlagt i ett år og Robinson har delt en video av frieriet i sosiale medier.

Videoen, som det kommende ekteparet har gitt VG tillatelse til å publisere, starter med at de to går ut i hagen etter å ha hørt et høyt smell og si-fi-musikk. Ute i hagen finner de det som ligner på en meteoritt med blått og lilla lys. For elleve år siden ble 31-åringen arrestert og sendt i fengsel for det brutale drapet på sin engelske samboer Meredith Kercher (21) i Italia. Saken fikk enorm oppmerksomhet over hele verden, inkludert i Norge. Etter fire år i fengsel ble Knox blankt frikjent, og hun flyttet tilbake til Seattle, hvor hun i dag jobber som journalist og lever lykkelig med sin nå forlovede.

Frieriet var planlagt i ett år og Robinson har delt en video av frieriet i sosiale medier.

Videoen, som det kommende ekteparet har gitt VG tillatelse til å publisere, starter med at de to går ut i hagen etter å ha hørt et høyt smell og si-fi-musikk. Ute i hagen finner de det som ligner på en meteoritt med blått og lilla lys. For elleve år siden ble 31-åringen arrestert og sendt i fengsel for det brutale drapet på sin engelske samboer Meredith Kercher (21) i Italia. Saken fikk enorm oppmerksomhet over hele verden, inkludert i Norge. Etter fire år i fengsel ble Knox blankt frikjent, og hun flyttet tilbake til Seattle, hvor hun i dag jobber som journalist og lever lykkelig med sin nå forlovede.

Frieriet var planlagt i ett år og Robinson har delt en video av frieriet i sosiale medier.

Videoen, som det kommende ekteparet har gitt VG tillatelse til å publisere, starter med at de to går ut i hagen etter å ha hørt et høyt smell og si-fi-musikk. Ute i hagen finner de det som ligner på en meteoritt med blått og lilla lys. For elleve år siden ble 31-åringen arrestert og sendt i fengsel for det brutale drapet på sin engelske samboer Meredith Kercher (21) i Italia. Saken fikk enorm oppmerksomhet over hele verden, inkludert i Norge. Etter fire år i fengsel ble Knox blankt frikjent, og hun flyttet tilbake til Seattle, hvor hun i dag jobber som journalist og lever lykkelig med sin nå forlovede.

Frieriet var planlagt i ett år og Robinson har delt en video av frieriet i sosiale medier.

Videoen, som det kommende ekteparet har gitt VG tillatelse til å publisere, starter med at de to går ut i hagen etter å ha hørt et høyt smell og si-fi-musikk. Ute i hagen finner de det som ligner på en meteoritt med blått og lilla lys. ',
  '2018-11-18 16:44:33',
  'https://smp.vgc.no/v2/images/98072fb2-3816-4604-9c0c-9c52a9a098a9?fit=crop&h=667&w=1000&s=047e2019afdd7e4be931ad2f17b35e75c57a087e',
  'FORELSKET: Knox sier til VG at de gleder seg til å planlegge bryllupet. Bildet er tatt fra da VG møtte paret i Seattle tidligere i år. Foto: Thomas Nilsson / VG',
  3,
  1
),
(
  'Einar Nilsson vant «Skal vi danse»: - Utslått av følelser',
  'Finalistparene Jan Gunnar Solli (37) og Rikke Lund (23), sammen med Einar Nilsson (54) og Anette Stokke (30), kjempet mot hverandre i finalen lørdag kveld.

Nilsson fikk færrest dommerpoeng under finalen, men vant til slutt seernes stemmer.

– Jeg er helt utslått av følelser. Det er vanskelig å beskrive, forteller Nilsson til VG.
les også
Jan Gunnar Solli svarer på romanseryktene: – Gøy at folk er nysgjerrig

Det ble en tårevåt slutt på finalen.

– Hvis man ikke klarer å holde tilbake tårene på grunn av glede, så er det helt greit, sier Nilsson.

Jan Gunnar Solli stikker av med andreplassen.

– Det er litt surt å tape, men jeg er en god taper i dag, og føler meg som en vinner. Jeg unner Anette og Einar dette av hele mitt hjerte, sier Solli til VG.Finalistparene Jan Gunnar Solli (37) og Rikke Lund (23), sammen med Einar Nilsson (54) og Anette Stokke (30), kjempet mot hverandre i finalen lørdag kveld.

Nilsson fikk færrest dommerpoeng under finalen, men vant til slutt seernes stemmer.

– Jeg er helt utslått av følelser. Det er vanskelig å beskrive, forteller Nilsson til VG.
les også
Jan Gunnar Solli svarer på romanseryktene: – Gøy at folk er nysgjerrig

Det ble en tårevåt slutt på finalen.

– Hvis man ikke klarer å holde tilbake tårene på grunn av glede, så er det helt greit, sier Nilsson.

Jan Gunnar Solli stikker av med andreplassen.

– Det er litt surt å tape, men jeg er en god taper i dag, og føler meg som en vinner. Jeg unner Anette og Einar dette av hele mitt hjerte, sier Solli til VG.Finalistparene Jan Gunnar Solli (37) og Rikke Lund (23), sammen med Einar Nilsson (54) og Anette Stokke (30), kjempet mot hverandre i finalen lørdag kveld.

Nilsson fikk færrest dommerpoeng under finalen, men vant til slutt seernes stemmer.

– Jeg er helt utslått av følelser. Det er vanskelig å beskrive, forteller Nilsson til VG.
les også
Jan Gunnar Solli svarer på romanseryktene: – Gøy at folk er nysgjerrig

Det ble en tårevåt slutt på finalen.

– Hvis man ikke klarer å holde tilbake tårene på grunn av glede, så er det helt greit, sier Nilsson.

Jan Gunnar Solli stikker av med andreplassen.

– Det er litt surt å tape, men jeg er en god taper i dag, og føler meg som en vinner. Jeg unner Anette og Einar dette av hele mitt hjerte, sier Solli til VG.Finalistparene Jan Gunnar Solli (37) og Rikke Lund (23), sammen med Einar Nilsson (54) og Anette Stokke (30), kjempet mot hverandre i finalen lørdag kveld.

Nilsson fikk færrest dommerpoeng under finalen, men vant til slutt seernes stemmer.

– Jeg er helt utslått av følelser. Det er vanskelig å beskrive, forteller Nilsson til VG.
les også
Jan Gunnar Solli svarer på romanseryktene: – Gøy at folk er nysgjerrig

Det ble en tårevåt slutt på finalen.

– Hvis man ikke klarer å holde tilbake tårene på grunn av glede, så er det helt greit, sier Nilsson.

Jan Gunnar Solli stikker av med andreplassen.

– Det er litt surt å tape, men jeg er en god taper i dag, og føler meg som en vinner. Jeg unner Anette og Einar dette av hele mitt hjerte, sier Solli til VG.Finalistparene Jan Gunnar Solli (37) og Rikke Lund (23), sammen med Einar Nilsson (54) og Anette Stokke (30), kjempet mot hverandre i finalen lørdag kveld.

Nilsson fikk færrest dommerpoeng under finalen, men vant til slutt seernes stemmer.

– Jeg er helt utslått av følelser. Det er vanskelig å beskrive, forteller Nilsson til VG.
les også
Jan Gunnar Solli svarer på romanseryktene: – Gøy at folk er nysgjerrig

Det ble en tårevåt slutt på finalen.

– Hvis man ikke klarer å holde tilbake tårene på grunn av glede, så er det helt greit, sier Nilsson.

Jan Gunnar Solli stikker av med andreplassen.

– Det er litt surt å tape, men jeg er en god taper i dag, og føler meg som en vinner. Jeg unner Anette og Einar dette av hele mitt hjerte, sier Solli til VG.Finalistparene Jan Gunnar Solli (37) og Rikke Lund (23), sammen med Einar Nilsson (54) og Anette Stokke (30), kjempet mot hverandre i finalen lørdag kveld.

Nilsson fikk færrest dommerpoeng under finalen, men vant til slutt seernes stemmer.

– Jeg er helt utslått av følelser. Det er vanskelig å beskrive, forteller Nilsson til VG.
les også
Jan Gunnar Solli svarer på romanseryktene: – Gøy at folk er nysgjerrig

Det ble en tårevåt slutt på finalen.

– Hvis man ikke klarer å holde tilbake tårene på grunn av glede, så er det helt greit, sier Nilsson.

Jan Gunnar Solli stikker av med andreplassen.

– Det er litt surt å tape, men jeg er en god taper i dag, og føler meg som en vinner. Jeg unner Anette og Einar dette av hele mitt hjerte, sier Solli til VG.',
  '2018-11-18 16:44:33',
  'https://smp.vgc.no/v2/images/87391c26-11fe-4b00-8870-a341a2030790?fit=crop&h=979&w=1000&s=544beeb7e399b2bfaeb777ac7c328cea97c9111e',
  'ELLEVILL JUBEL: Einar Nilsson danset seg til topps i årets utgave av «Skal vi danse». Foto: Trond Solberg',
  3,
  1
),
(
  'Bak det beste brødet',
  'I forrige uke kunne du lese bolletriksene til Norges beste baker, Nikolai Meling.

Denne gangen har vi tatt en prat med Melings trener på baker- og konditorlandslaget, Erlend Løken Volden. Han er utdannet diplombaker, og jobber til daglig med produktutvikling hos W.B. Samson i Oslo. Nå deler han sine beste tips til hvordan man får best mulig resultat når man skal bake brød hjemme.

– Det er én ting som gjør et brød saftig, og det er nok væske. Neste steg er å ha lang nok modning. Det er de to grunnprinsippene i brødbaking, sier han.

Brødspesial: Seks forskjellige brød til hverdagen
Ikke spar på væsken
ERFAREN BAKER: Landslagstrener Erlend Løken Volden er utdannet diplombaker og jobber til daglig med produktutvikling hos W.B. Samson i Oslo. Foto: Baker- og konditorlandslaget.

ERFAREN BAKER: Landslagstrener Erlend Løken Volden er utdannet diplombaker og jobber til daglig med produktutvikling hos W.B. Samson i Oslo. Foto: Baker- og konditorlandslaget.

Volden mener at den hyppigste årsaken til at hjemmebakte brød blir tørre, er at folk tilsetter mel når de kjenner at deigen blir veldig klissete, fordi de ikke klarer å håndtere den.

– Men mange hadde nok blitt overrasket over hvor fin struktur deigen får hvis den bare elter lenge nok. Om du tilsetter mer mel, ber du om tørt og kompakt brød, sier han.

Les også: De beste bakebloggene fra «Hele Norge baker»

På væskemengden har han en tommelfingerregel: Han baker aldri et brød med under 70 prosent vann per totale melmengde. Og med riktig elting og prosess kan du faktisk nærme deg 90–100 prosent vann, forklarer han.

For å kunne ha nok væske i deigen, og for at den skal ha en elastisitet som tåler at den skal bruke lang tid på modningen, forklarer Volden at deigen må ha en god, strekkbar struktur. Og da må den være eltet riktig.

Les også: Hjemmelaget brød til matpakke
Strekktesten

– Eltingen er veldig viktig. Hvis du bare blander sammen uforholdsmessig mye vann med mel, vil det bare bli en løs, strukturløs suppe, sier han.

Han forklarer at glutenet, proteinet i melet, er det som binder opp væsken før deigen blir stekt. For at dette proteinet skal klare å binde opp så mye væske som mulig, må den være eltet lenge nok.

Dårlig tid? Sjekk ut Maria Skappels hjemmebakte brød uten elting

Dette kan du sjekke ved å utføre det Volden kaller gluten- eller strekktesten:

Strekk deigen, og lag en tyggisbobleaktig hinne av den. Hvis du får til det, er deigen eltet lenge nok.

– Men da snakker vi fin hvetedeig. Jo grovere deigen er, jo mindre potensial har glutenet, som sitter i de finere delene av melet. Du kan elte en grov deig også, den har bare ikke det samme potensialet, sier han.

– Hvis du beveger deg over 70 prosent vann per totale melmengde, holder du bare igjen noe av vannet i starten av eltingen og sper det inn til slutt, når du har fått eltet en god struktur på deigen, altså etter at du har utført strekktesten.

Les også: Dette brødet tar av på internett
Bruk lang tid på modningen

Volden forklarer videre at et godt, saftig og aromatisk brød helst bør modnes på kjøl over natten hvis du skal ha en god smaksutvikling i det. Dette kan du gjøre på to måter:

Alternativ 1: Modne deigen på vanlig måte, og form bakverkene du skal lage. Så setter du baksten på stekebrettene inn i kjøleskapet til dagen etter, i stedet for å sette det til heving med en gang. Neste dag tar du baksten ut, etterhever ferdig hvis de ikke har gjort seg ferdig på kjøl, og steker av.

Les også: Arbeiderpartiet vil innføre skolemat

Alternativ 2: Sett hele deigen til modning i kjøleskapet til neste dag. Ta den ut og la den stå ute litt til hvis den ikke har doblet seg helt. Form baksten på vanlig måte, etterhev og stek.

Baker du over kjøl på denne måten, får du en helt annen smaksutvikling og struktur på baksten. Og du vil merke stor forskjell, lover Volden.

– Jeg ser at mange hjemmebakst-oppskrifter snakker om lunket vann og høye gjærmengder. Hvis du både har i for mye mel, og i tillegg bruker for kort tid med høy temperatur, får du hverken maksimal smaksutvikling eller saftighet. Begge de to tingene er veldig viktig.

Les også: Fem enkle bakverk alle kan få til
Kaldt vann og mindre gjær

Volden har derfor også en tommelfingerregel for gjærmengden i brøddeigen: Han overskrider aldri 1,5 prosent gjær per totale mengde mel (4 prosent på hvetebakst).

– Hvis du bikker over tre prosent skjer ting så fort i deigen at den ikke får tid til å utvikle noe smak, ikke engang hvis du setter den på kjøl. Den vil bare reise av gårde.

Hvetebakst: Fem fristende bakverk fra Lise Finckenhagen

Deig-temperaturen etter elting bør ifølge Volden helst ikke være over 24–25 grader. Selv bruker han alltid kaldt vann når han skal elte deigen.

– Når du elter dannes det friksjon. Hvis du begynner med det klassiske 37-gradersvannet blir det altfor varmt, og ting skjer altfor fort.

– Alt dette høres kanskje litt teknisk ut, men gjør deg selv en tjeneste hvis du vil ha god bakst, og gå oppskriften din litt etter i sømmene med disse hovedreglene, og du vil ha bedre sjanse for å lykkes, avslutter han.',
  '2018-11-18 16:44:33',
  'https://1.vgc.no/drpublish/images/article/2018/11/14/24492502/1/big_20/2605802.jpg',
  '
GODE BRØDTIPS: Trener på det norske baker- og konditorlandslaget, Erlend Løken Volden, gir sine beste tips til hvordan du unngår tørre og kompakte brød. Foto: Javad M. Parsa/VG
',
  1,
  1
),
(
  ' Her ligger ubåten på havets bunn',
  'Den argentinske marinen har gått ut med bilder som viser deler av ubåten, som ble funnet ett år etter at den forsvant sporløst. Nyheten mange pårørende hadde ventet på kom tidlig lørdag morgen: ARA San Juan var funnet på 920 meters dyp, omtrent 600 kilometer øst for den argentinske havnen Comodoro Rivadavia.

En av de pårørende til VG: Vi brast i gråt da vi fikk nyheten

Ubåten ble funnet av en fjernstyrt miniubåt (AUV) fra det norske forskningsskipet «Seabed Constructor». Skipet har blitt brukt i den omfattende leteaksjonen ledet av det amerikanske selskapet Ocean Infinity, som i høst ble hyret inn av den argentinske marinen.

Ifølge marinen imploderte ubåten på grunn av høyt vanntrykk, og sent lørdag kveld gikk de ut med bilder som viser deler av vraket (se billedgalleri øverst i saken).

Det er for lengst slått fast at ingen av de 44 i mannskapet om bord overlevde, selv om argentinske myndigheter så langt ikke har sagt om noen av de omkomne er funnet. Blant dem som mistet livet var landets første kvinnelige ubåtoffiser, Eliana Maria Krawczyk. Nyheten mange pårørende hadde ventet på kom tidlig lørdag morgen: ARA San Juan var funnet på 920 meters dyp, omtrent 600 kilometer øst for den argentinske havnen Comodoro Rivadavia.

En av de pårørende til VG: Vi brast i gråt da vi fikk nyheten

Ubåten ble funnet av en fjernstyrt miniubåt (AUV) fra det norske forskningsskipet «Seabed Constructor». Skipet har blitt brukt i den omfattende leteaksjonen ledet av det amerikanske selskapet Ocean Infinity, som i høst ble hyret inn av den argentinske marinen.

Ifølge marinen imploderte ubåten på grunn av høyt vanntrykk, og sent lørdag kveld gikk de ut med bilder som viser deler av vraket (se billedgalleri øverst i saken).

Det er for lengst slått fast at ingen av de 44 i mannskapet om bord overlevde, selv om argentinske myndigheter så langt ikke har sagt om noen av de omkomne er funnet. Blant dem som mistet livet var landets første kvinnelige ubåtoffiser, Eliana Maria Krawczyk. Nyheten mange pårørende hadde ventet på kom tidlig lørdag morgen: ARA San Juan var funnet på 920 meters dyp, omtrent 600 kilometer øst for den argentinske havnen Comodoro Rivadavia.

En av de pårørende til VG: Vi brast i gråt da vi fikk nyheten

Ubåten ble funnet av en fjernstyrt miniubåt (AUV) fra det norske forskningsskipet «Seabed Constructor». Skipet har blitt brukt i den omfattende leteaksjonen ledet av det amerikanske selskapet Ocean Infinity, som i høst ble hyret inn av den argentinske marinen.

Ifølge marinen imploderte ubåten på grunn av høyt vanntrykk, og sent lørdag kveld gikk de ut med bilder som viser deler av vraket (se billedgalleri øverst i saken).

Det er for lengst slått fast at ingen av de 44 i mannskapet om bord overlevde, selv om argentinske myndigheter så langt ikke har sagt om noen av de omkomne er funnet. Blant dem som mistet livet var landets første kvinnelige ubåtoffiser, Eliana Maria Krawczyk. Nyheten mange pårørende hadde ventet på kom tidlig lørdag morgen: ARA San Juan var funnet på 920 meters dyp, omtrent 600 kilometer øst for den argentinske havnen Comodoro Rivadavia.

En av de pårørende til VG: Vi brast i gråt da vi fikk nyheten

Ubåten ble funnet av en fjernstyrt miniubåt (AUV) fra det norske forskningsskipet «Seabed Constructor». Skipet har blitt brukt i den omfattende leteaksjonen ledet av det amerikanske selskapet Ocean Infinity, som i høst ble hyret inn av den argentinske marinen.

Ifølge marinen imploderte ubåten på grunn av høyt vanntrykk, og sent lørdag kveld gikk de ut med bilder som viser deler av vraket (se billedgalleri øverst i saken).

Det er for lengst slått fast at ingen av de 44 i mannskapet om bord overlevde, selv om argentinske myndigheter så langt ikke har sagt om noen av de omkomne er funnet. Blant dem som mistet livet var landets første kvinnelige ubåtoffiser, Eliana Maria Krawczyk. Nyheten mange pårørende hadde ventet på kom tidlig lørdag morgen: ARA San Juan var funnet på 920 meters dyp, omtrent 600 kilometer øst for den argentinske havnen Comodoro Rivadavia.

En av de pårørende til VG: Vi brast i gråt da vi fikk nyheten

Ubåten ble funnet av en fjernstyrt miniubåt (AUV) fra det norske forskningsskipet «Seabed Constructor». Skipet har blitt brukt i den omfattende leteaksjonen ledet av det amerikanske selskapet Ocean Infinity, som i høst ble hyret inn av den argentinske marinen.

Ifølge marinen imploderte ubåten på grunn av høyt vanntrykk, og sent lørdag kveld gikk de ut med bilder som viser deler av vraket (se billedgalleri øverst i saken).

Det er for lengst slått fast at ingen av de 44 i mannskapet om bord overlevde, selv om argentinske myndigheter så langt ikke har sagt om noen av de omkomne er funnet. Blant dem som mistet livet var landets første kvinnelige ubåtoffiser, Eliana Maria Krawczyk. Nyheten mange pårørende hadde ventet på kom tidlig lørdag morgen: ARA San Juan var funnet på 920 meters dyp, omtrent 600 kilometer øst for den argentinske havnen Comodoro Rivadavia.

En av de pårørende til VG: Vi brast i gråt da vi fikk nyheten

Ubåten ble funnet av en fjernstyrt miniubåt (AUV) fra det norske forskningsskipet «Seabed Constructor». Skipet har blitt brukt i den omfattende leteaksjonen ledet av det amerikanske selskapet Ocean Infinity, som i høst ble hyret inn av den argentinske marinen.

Ifølge marinen imploderte ubåten på grunn av høyt vanntrykk, og sent lørdag kveld gikk de ut med bilder som viser deler av vraket (se billedgalleri øverst i saken).

Det er for lengst slått fast at ingen av de 44 i mannskapet om bord overlevde, selv om argentinske myndigheter så langt ikke har sagt om noen av de omkomne er funnet. Blant dem som mistet livet var landets første kvinnelige ubåtoffiser, Eliana Maria Krawczyk. Nyheten mange pårørende hadde ventet på kom tidlig lørdag morgen: ARA San Juan var funnet på 920 meters dyp, omtrent 600 kilometer øst for den argentinske havnen Comodoro Rivadavia.

En av de pårørende til VG: Vi brast i gråt da vi fikk nyheten

Ubåten ble funnet av en fjernstyrt miniubåt (AUV) fra det norske forskningsskipet «Seabed Constructor». Skipet har blitt brukt i den omfattende leteaksjonen ledet av det amerikanske selskapet Ocean Infinity, som i høst ble hyret inn av den argentinske marinen.

Ifølge marinen imploderte ubåten på grunn av høyt vanntrykk, og sent lørdag kveld gikk de ut med bilder som viser deler av vraket (se billedgalleri øverst i saken).

Det er for lengst slått fast at ingen av de 44 i mannskapet om bord overlevde, selv om argentinske myndigheter så langt ikke har sagt om noen av de omkomne er funnet. Blant dem som mistet livet var landets første kvinnelige ubåtoffiser, Eliana Maria Krawczyk. ',
  '2018-11-18 16:44:33',
  'https://smp.vgc.no/v2/images/486209dc-6f42-4ab9-b0a3-0fc50cb55814?fit=crop&h=406&w=537&s=31466f6ca0c8fb1ef15cd80ff5a1c90c49c4e090',
  'PÅ HAVDYPET: Her ser man deler av ubåten ARA San Juan, som lørdag morgen ble funnet på 920 meters dyp.',
  2,
  1
),
(
  'Prins Charles fyller 70 år i dag: Deler sjeldent familiebilde',
  'Clarence House postet tirsdag kveld et nytt bilde av prins Charles med kona Camilla (71), prinsesønnene William (36) og Harry (34), samt deres koner Kate (36) og Meghan (37). Med på bildet er også prins William og hertuginne Kates tre barn.

Anledningen er at prins Charles fyller 70 år i dag.

Det offisielle bildet står i kontrast til de tradisjonelle familiebildene man normalt ser fra hoffet. Selv om familiemedlemmene er festkledd, gir det et mye varmere og kjærligere inntrykk enn de normalt stive, oppstilte positurene til de kongelige.

Prins Charles har det eldste barnebarnet – Prins George (5) – på fanget, mens Camilla sitter med prinsesse Charlotte (3) i armkroken. Latteren satt tydelig løst under fotograferingen.
les også
Hva slags konge blir Charles?

– Det var ganske så spesielt å forevige et så uformelt og avslappet familieportrett, sier fotografen Chris Jackson ifølge Huffington Post.

Familiens yngste tilskudd, prins Louis (seks mdr.), sitter på armen til mamma Kate. At hertuginne Meghan også har et barn i magen, er ikke mulig å skimte på bildet. Trolig er bildet tatt i hagen til Clarence House tidligere i høst.

Clarence House er residensen til Charles og Camilla i London.Clarence House postet tirsdag kveld et nytt bilde av prins Charles med kona Camilla (71), prinsesønnene William (36) og Harry (34), samt deres koner Kate (36) og Meghan (37). Med på bildet er også prins William og hertuginne Kates tre barn.

Anledningen er at prins Charles fyller 70 år i dag.

Det offisielle bildet står i kontrast til de tradisjonelle familiebildene man normalt ser fra hoffet. Selv om familiemedlemmene er festkledd, gir det et mye varmere og kjærligere inntrykk enn de normalt stive, oppstilte positurene til de kongelige.

Prins Charles har det eldste barnebarnet – Prins George (5) – på fanget, mens Camilla sitter med prinsesse Charlotte (3) i armkroken. Latteren satt tydelig løst under fotograferingen.
les også
Hva slags konge blir Charles?

– Det var ganske så spesielt å forevige et så uformelt og avslappet familieportrett, sier fotografen Chris Jackson ifølge Huffington Post.

Familiens yngste tilskudd, prins Louis (seks mdr.), sitter på armen til mamma Kate. At hertuginne Meghan også har et barn i magen, er ikke mulig å skimte på bildet. Trolig er bildet tatt i hagen til Clarence House tidligere i høst.

Clarence House er residensen til Charles og Camilla i London.Clarence House postet tirsdag kveld et nytt bilde av prins Charles med kona Camilla (71), prinsesønnene William (36) og Harry (34), samt deres koner Kate (36) og Meghan (37). Med på bildet er også prins William og hertuginne Kates tre barn.

Anledningen er at prins Charles fyller 70 år i dag.

Det offisielle bildet står i kontrast til de tradisjonelle familiebildene man normalt ser fra hoffet. Selv om familiemedlemmene er festkledd, gir det et mye varmere og kjærligere inntrykk enn de normalt stive, oppstilte positurene til de kongelige.

Prins Charles har det eldste barnebarnet – Prins George (5) – på fanget, mens Camilla sitter med prinsesse Charlotte (3) i armkroken. Latteren satt tydelig løst under fotograferingen.
les også
Hva slags konge blir Charles?

– Det var ganske så spesielt å forevige et så uformelt og avslappet familieportrett, sier fotografen Chris Jackson ifølge Huffington Post.

Familiens yngste tilskudd, prins Louis (seks mdr.), sitter på armen til mamma Kate. At hertuginne Meghan også har et barn i magen, er ikke mulig å skimte på bildet. Trolig er bildet tatt i hagen til Clarence House tidligere i høst.

Clarence House er residensen til Charles og Camilla i London.Clarence House postet tirsdag kveld et nytt bilde av prins Charles med kona Camilla (71), prinsesønnene William (36) og Harry (34), samt deres koner Kate (36) og Meghan (37). Med på bildet er også prins William og hertuginne Kates tre barn.

Anledningen er at prins Charles fyller 70 år i dag.

Det offisielle bildet står i kontrast til de tradisjonelle familiebildene man normalt ser fra hoffet. Selv om familiemedlemmene er festkledd, gir det et mye varmere og kjærligere inntrykk enn de normalt stive, oppstilte positurene til de kongelige.

Prins Charles har det eldste barnebarnet – Prins George (5) – på fanget, mens Camilla sitter med prinsesse Charlotte (3) i armkroken. Latteren satt tydelig løst under fotograferingen.
les også
Hva slags konge blir Charles?

– Det var ganske så spesielt å forevige et så uformelt og avslappet familieportrett, sier fotografen Chris Jackson ifølge Huffington Post.

Familiens yngste tilskudd, prins Louis (seks mdr.), sitter på armen til mamma Kate. At hertuginne Meghan også har et barn i magen, er ikke mulig å skimte på bildet. Trolig er bildet tatt i hagen til Clarence House tidligere i høst.

Clarence House er residensen til Charles og Camilla i London.Clarence House postet tirsdag kveld et nytt bilde av prins Charles med kona Camilla (71), prinsesønnene William (36) og Harry (34), samt deres koner Kate (36) og Meghan (37). Med på bildet er også prins William og hertuginne Kates tre barn.

Anledningen er at prins Charles fyller 70 år i dag.

Det offisielle bildet står i kontrast til de tradisjonelle familiebildene man normalt ser fra hoffet. Selv om familiemedlemmene er festkledd, gir det et mye varmere og kjærligere inntrykk enn de normalt stive, oppstilte positurene til de kongelige.

Prins Charles har det eldste barnebarnet – Prins George (5) – på fanget, mens Camilla sitter med prinsesse Charlotte (3) i armkroken. Latteren satt tydelig løst under fotograferingen.
les også
Hva slags konge blir Charles?

– Det var ganske så spesielt å forevige et så uformelt og avslappet familieportrett, sier fotografen Chris Jackson ifølge Huffington Post.

Familiens yngste tilskudd, prins Louis (seks mdr.), sitter på armen til mamma Kate. At hertuginne Meghan også har et barn i magen, er ikke mulig å skimte på bildet. Trolig er bildet tatt i hagen til Clarence House tidligere i høst.

Clarence House er residensen til Charles og Camilla i London.',
  '2018-11-17 16:44:33',
  'https://smp.vgc.no/v2/images/1dd35c69-3124-4518-b274-d6a9eafb1d6c?fit=crop&h=1363&w=1000&s=d15a5a5e6fcbc577933aa8d5ad465dd886be873a',
  'JUBILANT: Prins Charles fyller 70 år i dag. Det markerer han ved å posere smilende med sine aller nærmeste. Foto: Chris Jackson / Reuters',
  3,
  1
),
(
  'Trump besøkte nedbrent by: – Trist å se ',
  'Her står Donald Trump i ruinene av byen Paradise etter branninfernoet nord i California. Samtidig fortsetter dødstallet å stige.– Det er veldig trist å se, sa presidenten mens han stirret utover et nedbrent nabolag.
les også
De enorme brannene i California: Jobber desperat for å finne overlevende

– De forteller meg at dette ikke engang er like ille som det er i andre områder. De sier at noen områder er verre enn dette, at det bare er aske igjen, fortsatte Trump.

Les også: Mistet hjemmet for andre gang i skogbrann

Det hissige branninfernoet, som har fått navnet «Camp Fire», er den dødeligste brannen i Californias historie.

Natt til søndag norsk tid bekreftet politiet at ytterligere fem personer er funnet døde i ruinene, og at dødstallet dermed er oppe i 76. I tillegg er nærmere 1300 personer savnet. – Det er veldig trist å se, sa presidenten mens han stirret utover et nedbrent nabolag.
les også
De enorme brannene i California: Jobber desperat for å finne overlevende

– De forteller meg at dette ikke engang er like ille som det er i andre områder. De sier at noen områder er verre enn dette, at det bare er aske igjen, fortsatte Trump.

Les også: Mistet hjemmet for andre gang i skogbrann

Det hissige branninfernoet, som har fått navnet «Camp Fire», er den dødeligste brannen i Californias historie.

Natt til søndag norsk tid bekreftet politiet at ytterligere fem personer er funnet døde i ruinene, og at dødstallet dermed er oppe i 76. I tillegg er nærmere 1300 personer savnet. – Det er veldig trist å se, sa presidenten mens han stirret utover et nedbrent nabolag.
les også
De enorme brannene i California: Jobber desperat for å finne overlevende

– De forteller meg at dette ikke engang er like ille som det er i andre områder. De sier at noen områder er verre enn dette, at det bare er aske igjen, fortsatte Trump.

Les også: Mistet hjemmet for andre gang i skogbrann

Det hissige branninfernoet, som har fått navnet «Camp Fire», er den dødeligste brannen i Californias historie.

Natt til søndag norsk tid bekreftet politiet at ytterligere fem personer er funnet døde i ruinene, og at dødstallet dermed er oppe i 76. I tillegg er nærmere 1300 personer savnet. – Det er veldig trist å se, sa presidenten mens han stirret utover et nedbrent nabolag.
les også
De enorme brannene i California: Jobber desperat for å finne overlevende

– De forteller meg at dette ikke engang er like ille som det er i andre områder. De sier at noen områder er verre enn dette, at det bare er aske igjen, fortsatte Trump.

Les også: Mistet hjemmet for andre gang i skogbrann

Det hissige branninfernoet, som har fått navnet «Camp Fire», er den dødeligste brannen i Californias historie.

Natt til søndag norsk tid bekreftet politiet at ytterligere fem personer er funnet døde i ruinene, og at dødstallet dermed er oppe i 76. I tillegg er nærmere 1300 personer savnet. – Det er veldig trist å se, sa presidenten mens han stirret utover et nedbrent nabolag.
les også
De enorme brannene i California: Jobber desperat for å finne overlevende

– De forteller meg at dette ikke engang er like ille som det er i andre områder. De sier at noen områder er verre enn dette, at det bare er aske igjen, fortsatte Trump.

Les også: Mistet hjemmet for andre gang i skogbrann

Det hissige branninfernoet, som har fått navnet «Camp Fire», er den dødeligste brannen i Californias historie.

Natt til søndag norsk tid bekreftet politiet at ytterligere fem personer er funnet døde i ruinene, og at dødstallet dermed er oppe i 76. I tillegg er nærmere 1300 personer savnet. ',
  '2018-11-18 16:45:33',
  'https://smp.vgc.no/v2/images/002dcef8-559c-4364-806d-18adbcbba277?fit=crop&h=657&w=1000&s=81db7620db7b0b16571233b2df2d35b9cf9c60ba',
  'BESØKER BRANNOFRE: USAs president Donald Trump på plass i California. Her sammen med ordfører Jody Jones. ',
  4,
  1
),
(
  'Reagerer kraftig på Virkes bruk av brannvesenet i reklame: – Utrolig frekt',
  'Hovedorganisasjonen Virke bruker en av Oslo brann- og redningsetats brannbiler i ny reklame – til tross for at de fikk et klart nei.Det har fått Lars Magne Hovtun, informasjonssjef i Oslo brann- og redningsetat til å reagere.

– Jeg fikk en henvendelse fra Virke forrige uke, som ble avslått både muntlig og skriftlig. Begrunnet med at vi ikke ønsker å være en del av slike kampanjer, forteller Hovtun til VG.

Overraskelsen var derfor stor da en av Hovedbrannstasjonens stigebiler dukket opp i en helsides reklame i lørdagsutgaven av i alle fall både Dagens Næringsliv og VG.
les også
TV-kanaler dropper omstridt kampanjevideo
– Spekulativt

– Jeg antok avslaget mitt var lett å forstå, og ble derfor veldig overrasket av å se brannbilen vår i en slik reklame. Jeg synes det er utrolig frekt å ta seg til rette på denne måten. Særlig når retorikken i budskapet deres er helt feil.

Budskapet i reklamen er at 350-kronersgrensen, som tilsier at alle sendinger fra utlandet til en verdi over 350 kroner blir toll- og avgiftsbelagt, bør fjernes. Virke argumenterer for at grensen gir Norge mindre inntekter, «som for eksempel kunne blitt brukt på å styrke brannvesenet ytterligere». Det har fått Lars Magne Hovtun, informasjonssjef i Oslo brann- og redningsetat til å reagere.

– Jeg fikk en henvendelse fra Virke forrige uke, som ble avslått både muntlig og skriftlig. Begrunnet med at vi ikke ønsker å være en del av slike kampanjer, forteller Hovtun til VG.

Overraskelsen var derfor stor da en av Hovedbrannstasjonens stigebiler dukket opp i en helsides reklame i lørdagsutgaven av i alle fall både Dagens Næringsliv og VG.
les også
TV-kanaler dropper omstridt kampanjevideo
– Spekulativt

– Jeg antok avslaget mitt var lett å forstå, og ble derfor veldig overrasket av å se brannbilen vår i en slik reklame. Jeg synes det er utrolig frekt å ta seg til rette på denne måten. Særlig når retorikken i budskapet deres er helt feil.

Budskapet i reklamen er at 350-kronersgrensen, som tilsier at alle sendinger fra utlandet til en verdi over 350 kroner blir toll- og avgiftsbelagt, bør fjernes. Virke argumenterer for at grensen gir Norge mindre inntekter, «som for eksempel kunne blitt brukt på å styrke brannvesenet ytterligere». Det har fått Lars Magne Hovtun, informasjonssjef i Oslo brann- og redningsetat til å reagere.

– Jeg fikk en henvendelse fra Virke forrige uke, som ble avslått både muntlig og skriftlig. Begrunnet med at vi ikke ønsker å være en del av slike kampanjer, forteller Hovtun til VG.

Overraskelsen var derfor stor da en av Hovedbrannstasjonens stigebiler dukket opp i en helsides reklame i lørdagsutgaven av i alle fall både Dagens Næringsliv og VG.
les også
TV-kanaler dropper omstridt kampanjevideo
– Spekulativt

– Jeg antok avslaget mitt var lett å forstå, og ble derfor veldig overrasket av å se brannbilen vår i en slik reklame. Jeg synes det er utrolig frekt å ta seg til rette på denne måten. Særlig når retorikken i budskapet deres er helt feil.

Budskapet i reklamen er at 350-kronersgrensen, som tilsier at alle sendinger fra utlandet til en verdi over 350 kroner blir toll- og avgiftsbelagt, bør fjernes. Virke argumenterer for at grensen gir Norge mindre inntekter, «som for eksempel kunne blitt brukt på å styrke brannvesenet ytterligere». Det har fått Lars Magne Hovtun, informasjonssjef i Oslo brann- og redningsetat til å reagere.

– Jeg fikk en henvendelse fra Virke forrige uke, som ble avslått både muntlig og skriftlig. Begrunnet med at vi ikke ønsker å være en del av slike kampanjer, forteller Hovtun til VG.

Overraskelsen var derfor stor da en av Hovedbrannstasjonens stigebiler dukket opp i en helsides reklame i lørdagsutgaven av i alle fall både Dagens Næringsliv og VG.
les også
TV-kanaler dropper omstridt kampanjevideo
– Spekulativt

– Jeg antok avslaget mitt var lett å forstå, og ble derfor veldig overrasket av å se brannbilen vår i en slik reklame. Jeg synes det er utrolig frekt å ta seg til rette på denne måten. Særlig når retorikken i budskapet deres er helt feil.

Budskapet i reklamen er at 350-kronersgrensen, som tilsier at alle sendinger fra utlandet til en verdi over 350 kroner blir toll- og avgiftsbelagt, bør fjernes. Virke argumenterer for at grensen gir Norge mindre inntekter, «som for eksempel kunne blitt brukt på å styrke brannvesenet ytterligere». Det har fått Lars Magne Hovtun, informasjonssjef i Oslo brann- og redningsetat til å reagere.

– Jeg fikk en henvendelse fra Virke forrige uke, som ble avslått både muntlig og skriftlig. Begrunnet med at vi ikke ønsker å være en del av slike kampanjer, forteller Hovtun til VG.

Overraskelsen var derfor stor da en av Hovedbrannstasjonens stigebiler dukket opp i en helsides reklame i lørdagsutgaven av i alle fall både Dagens Næringsliv og VG.
les også
TV-kanaler dropper omstridt kampanjevideo
– Spekulativt

– Jeg antok avslaget mitt var lett å forstå, og ble derfor veldig overrasket av å se brannbilen vår i en slik reklame. Jeg synes det er utrolig frekt å ta seg til rette på denne måten. Særlig når retorikken i budskapet deres er helt feil.

Budskapet i reklamen er at 350-kronersgrensen, som tilsier at alle sendinger fra utlandet til en verdi over 350 kroner blir toll- og avgiftsbelagt, bør fjernes. Virke argumenterer for at grensen gir Norge mindre inntekter, «som for eksempel kunne blitt brukt på å styrke brannvesenet ytterligere». Det har fått Lars Magne Hovtun, informasjonssjef i Oslo brann- og redningsetat til å reagere.

– Jeg fikk en henvendelse fra Virke forrige uke, som ble avslått både muntlig og skriftlig. Begrunnet med at vi ikke ønsker å være en del av slike kampanjer, forteller Hovtun til VG.

Overraskelsen var derfor stor da en av Hovedbrannstasjonens stigebiler dukket opp i en helsides reklame i lørdagsutgaven av i alle fall både Dagens Næringsliv og VG.
les også
TV-kanaler dropper omstridt kampanjevideo
– Spekulativt

– Jeg antok avslaget mitt var lett å forstå, og ble derfor veldig overrasket av å se brannbilen vår i en slik reklame. Jeg synes det er utrolig frekt å ta seg til rette på denne måten. Særlig når retorikken i budskapet deres er helt feil.

Budskapet i reklamen er at 350-kronersgrensen, som tilsier at alle sendinger fra utlandet til en verdi over 350 kroner blir toll- og avgiftsbelagt, bør fjernes. Virke argumenterer for at grensen gir Norge mindre inntekter, «som for eksempel kunne blitt brukt på å styrke brannvesenet ytterligere».  ',
  '2018-11-18 06:44:33',
  'https://smp.vgc.no/v2/images/a7283bfc-6198-43b3-8cc8-3027bc205806?fit=crop&h=667&w=1000&s=5d36137476acf94fb640d9dcd3ed5eed9e110373',
  ' REAGERER: Informasjonssjef i Oslo brann- og redningsetat, Lars Magne Hovtun. Foto: Larsen, Håkon Mosvold / NTB scanpix',
  5,
  1
),
(
  'VG trapper opp e-sport-satsingen: Inngår samarbeid med Telenorligaen',
  'Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.Neste helg får du se noen av Norges beste e-sportsspillere eksklusivt på VGTV, når alt skal avgjøres i finalerundene av League of Legends, Rocket League og Counter-Strike: Global Offensive.',
  '2018-11-18 16:44:33',
  'https://smp.vgc.no/v2/images/29f6854b-8b1a-4ed9-ac38-7e2c42fc157f?fit=crop&h=667&w=1000&s=8bb949022b10a1021f4f7f6a6192447e9ff44795',
  'DUKET FOR FEST: Alt ligger til rette for en ellevill avslutning på Telenorligaen i Lillehammer. Foto: Eivind Von Døhlen, Telenorligaen',
  1,
  1
),
(
  '«Farmen»-Lene: – Jeg kunne absolutt forelsket meg i en gutt',
  'Lene Sleperud hadde aldri hatt kvinnelig kjæreste før hun møtte Tonje. Ingen av dem utelukker at de kan falle for en mann igjen. Tonje Frøystad Garvik (29) og Lene Sleperud (29) har i snart åtte uker levd på «Farmen»-gården uten å røpe for de andre deltagerne at de er faktisk er kjærester, noe som har skapt reaksjoner blant enkelte. Vel hjemme igjen i Oslo har de fått tatt igjen litt av den kjærligheten de måtte skjule inne på gården.

Paret har vært kjærester i drøye to år, og møttes gjorde de på festivalen Oslo Pride.

– Vi brukte 15 måneder fra vi møttes til vi så på hverandre og sa «Ja, vi er kjærester», forteller Tonje og Lene til VG.
les også
«Farmen»-deltagerne drakk seg fulle og rømte gården
– Falt for spenningen

For mens Tonje har vært klar på legningen sin i mange, mange år, har det ikke vært helt det samme for Lene. Tonje er nemlig hennes første kvinnelige partner.

– Det første jeg falt for var spenningen mellom oss. Det å ville vite hva som lå bak det pene ansiktet hennes. Hun ga så mye av seg selv, og hadde så mye energi. Jeg fikk en positiv energi når jeg var sammen med henne, forteller Lene.  – Lene visste jo om min legning, men var jo ikke helt der selv. Første gang jeg tenkte at dette faktisk kunne bli noe var da Lene hadde julebord hjemme hos seg selv og sa at hvis hun var fanget på en øde øy, ville hun vært der med meg. Fordi hun da visste at hun kom til å ha det fint. Det vekket noe i meg, sier Tonje.
– Aldri sett meg så forelsket

Til tross for sitt lesbiske partnerskap, legger ikke Lene skjul på at hun kunne falt for en gutt igjen.  – Lene visste jo om min legning, men var jo ikke helt der selv. Første gang jeg tenkte at dette faktisk kunne bli noe var da Lene hadde julebord hjemme hos seg selv og sa at hvis hun var fanget på en øde øy, ville hun vært der med meg. Fordi hun da visste at hun kom til å ha det fint. Det vekket noe i meg, sier Tonje.
– Aldri sett meg så forelsket

Til tross for sitt lesbiske partnerskap, legger ikke Lene skjul på at hun kunne falt for en gutt igjen.  – Lene visste jo om min legning, men var jo ikke helt der selv. Første gang jeg tenkte at dette faktisk kunne bli noe var da Lene hadde julebord hjemme hos seg selv og sa at hvis hun var fanget på en øde øy, ville hun vært der med meg. Fordi hun da visste at hun kom til å ha det fint. Det vekket noe i meg, sier Tonje.
– Aldri sett meg så forelsket

Til tross for sitt lesbiske partnerskap, legger ikke Lene skjul på at hun kunne falt for en gutt igjen.  – Lene visste jo om min legning, men var jo ikke helt der selv. Første gang jeg tenkte at dette faktisk kunne bli noe var da Lene hadde julebord hjemme hos seg selv og sa at hvis hun var fanget på en øde øy, ville hun vært der med meg. Fordi hun da visste at hun kom til å ha det fint. Det vekket noe i meg, sier Tonje.
– Aldri sett meg så forelsket

Til tross for sitt lesbiske partnerskap, legger ikke Lene skjul på at hun kunne falt for en gutt igjen. ',
  '2018-11-18 16:44:33',
  'https://smp.vgc.no/v2/images/e0c86a5f-3c66-46be-b4c7-7f573a5d92a8?fit=crop&h=841&w=1000&s=9e33046f9346628f9efbef415773251bf060e1a4',
  'STORMFORELSKET: Tonje Frøystad Garvik og Lene Sleperud ble kjærester for drøye to år siden, men har klart å skjule forholdet inne på «Farmen». Foto: Trond Solberg, VG',
  1,
  1
),
(
  'Mette-Marit strålte i gjenbrukskjole på kongelig bursdagsfest',
  'Både kronprinsessen (45) og dronning Sonja (81) valgte likt som forrige gang de møtte de britiske kongelige.

Onsdag kveld var 70 gjester med følge invitert til London og Buckingham Palace for å feire prins Charles 70-årsdag.

Kong Harald (81), dronning Sonja, kronprins Haakon (45) og kronprinsesse Mette-Marit var alle blant de celebre gjestene, og de tok seg tid til å stille opp for fotografene for noen bilder utenfor den private festen.

Forrige gang de norske kongelige møtte de britiske kongelige var under et statsbesøk i Norge tidligere i år.

Den gangen var det duket for middag på Slottet, og det kan se ut til at de norske kongelige lot seg inspirere på antrekksfronten.

Alle fire var nemlig kledd i nøyaktig det samme også denne gangen.Forrige gang ble Mette-Marits kjole beskrevet som en skikkelig innertier. Kjolen er designet av norske Tina Steffenakk Hermansen og merket TSH.

Denne gangen hadde Mette-Marit valgt å gå for sin signaturstyling med løse, myke Hollywood-krøller og lett sotede øyne.

Også TV 2s kongeblogger Kjell Arne Totland har lagt merke til at de norske kongelige har gått for gjenbruk.

– Dronning Sonjas utsøkte kappe-kreasjon i kardinal-lilla silketaft og blonde var ny til regjeringsjubileet i 2016, sier han og legger til:

– Og den gang som nå bærer dronning Sonja som en vakker gest en britisk praktbrosje med store dråpeperler etter dronning Maud midt på brystet – nettopp slik Maud også alltid gjorde.Blant gjestene var også selvfølgelig prins Harry og kona hertuginne Meghan, samt prins William og hertuginne Kate. Siden det var et privat arrangement, er det ikke mange bilder av dem fra kveldens festligheter. På vei inn til festen kan man imidlertid skimte at de hadde pyntet seg med store diamantøredobber for anledningen, noe som får oppmerksomhet i britisk presse. ',
  '2018-11-18 16:44:33',
'https://1.vgc.no/drpublish/images/article/2018/11/15/24493357/1/normal-big_15/Kongeparet_i_London.jpg',
  'ARM I ARM: Her er kronprinsesse Mette-Marit og kronprins Haakon på vei til mottagelse og middag i i anledning 70-årsdagen til prins Charles. Foto: Nina E. Rangøy / NTB scanpix. ',
  3,
  1
),
(
  'Kapteinsrollen bør selvsagt diskuteres',
  'NICOSIA (VG) Siden Per Ciljan Skjelbred fikk jobben i 2013, deretter Stefan Johansen, har Norges landslagskaptein blitt byttet ut i 29 av 37 kamper vedkommende har startet. Da er det ikke så rart at vi diskuterer viktigheten av å ha kapteinen på banen.Lars Lagerbäck virket ikke veldig fornøyd med at temaet kom opp på søndagens pressekonferanse, og sa vel egentlig at vi i mediene – om det var sånn vi ønsket det – fikk holde på med vårt.

Men hva er galt med å diskutere viktigheten av å ha kapteinen på banen i samtlige 90 minutter? Hva er galt med å diskutere hvorvidt Stefan Johansen bør lede Norge mot Kypros mandag kveld i Nicosia?
Stefan Johansen får gjennomgå: – Skal dere begynne med det igjen?

Normalt sett pleier lagkapteinen å være blant de viktigste på laget, ikke bare utenfor banen, og en av de første på arket til landslagssjefen. Og Stefan Johansen innrømmet det selv på søndagens pressekonferanse: Han har spilt lite fotball i høst. Og det preger ham. Han holder ikke kampene, som han alltid har gjort tidligere. I tillegg har dette medført en liten skade. Så han trente ikke med laget søndag. Derfor er det også betimelig å diskutere plassen, og rollen, Johansen har. Igjen.

315 minutter med ligafotball har Stefan Johansen spilt i høst. På drøyt tre måneder har han spilt 29 prosent av Fulhams 1080 minutter.

I september, da høstens første landskamper kom, gikk det greit. I oktober, da de neste landskampene kom, var det allerede litt verre. Og i november, da mer enn tre måneder har gått, da merker både spiller og landslagssjef at nå preges både spiller og landslagsledelsen av lite spilletid på enkelte.
Lagerbäck bør vrake kapteinen sin

For Alexander Sørloth er det enda verre: 127 minutter har han spilt så langt. Det er bare litt mer enn én kamp av 12 mulige. Og vi ser at Sørloth er kamprusten også.

Det er helt naturlig.',
  '2018-11-18 16:44:33',
  'https://smp.vgc.no/v2/images/aff668be-2b5a-4f1e-bc0d-0241442e13ec?fit=crop&h=673&w=1000&s=fa40f43766eefb890a254426a85198291c71491d',
  'BETENKT: En halvskadet Stefan Johansen – landslagssjef Lars Lagerbäck og Tarik Elyounoussi på pressekonferansen søndag. Foto: Bjørn S. Delebekk',
  1,
  2
),
(
  '
Dette bør du vite om den fjerde barnesykdom
',
  'Den er ufarlig, men rammer tidlig. Slik vet du om barnet ditt er blitt smittet.

Den fjerde barnesykdom kan virke noe voldsom, men den er en mild og ufarlig virussykdom som går over av seg selv. Det kan likevel være vanskelig å vite hva skal man se etter, og hvordan man vet at det nettopp er den fjerde barnesykdom det er snakk om.



Så hva er egentlig denne sykdommen?



– Den fjerde barnesykdom er en virusinfeksjon som skyldes et virus i herpesfamilien. Den smitter ved lett dråpesmitte, forteller overlege og spesialist i barnesykdommer ved Folkehelseinstituttet, Margrethe Greve-Isdahl.



Les også: Derfor er trassalderen et positivt tegn



«Tredagersfeber»

Tiden fra barnet blir smittet til sykdommen bryter ut er 5 til 10 dager. Den fjerde barnesykdom kalles også «tredagersfeber».



– Grunnen til at man ofte kaller denne infeksjonen «tredagersfeber» er at barnet raskt får en relativt høy feber, opptil 40 grader, som typisk varer i tre dager. Når feberen faller etter noen dager, får barnet svakt rosa og småprikkete utslett over bryst og rygg, men det kan også forekomme andre steder, forteller Greve-Isdahl.



Hun legger til at man bør være oppmerksom på at små barn ofte kan få feberkramper i forbindelse med høy temperatur i kroppen.



– Hos noen barn vil den raskt stigende feberen kunne utløse feberkramper, men feberkrampene er knyttet til selve temperaturendringen, og ikke spesifikt til viruset, sier hun.



Les også: Hva er hånd-, fot- og munnsyke?



Går over av seg selv

Denne sykdommen er en svært vanlig tilstand, og den rammer mer eller mindre alle barn. De fleste opplever å bli smittet av den fjerde barnesykdom i løpet av de 2-3 første leveårene, og aller hyppigst rammer den mot slutten av det første leveåret.



– Feberen varer i noen få dager, typisk i tre dager, og når feberen faller vil barnet få utslett. Utslettet varer vanligvis i 1-4 dager. Hos immunfriske barn er dette en ukomplisert infeksjon i barndommen. Sykdommen går over av seg selv, og krever ingen spesifikk behandling, forteller Greve-Isdahl.



– Er det nødvendig å oppsøke lege?



– Nei, ikke så lenge allmenntilstanden er god.



Men Greve-Isdahl legger til at lege alltid skal oppsøkes dersom foreldrene er usikre, eller barnet har dårlig allmenntilstand.



– Dette gjelder ved all sykdom hos barn: Har de dårlig allmenntilstand, hvis de ikke vil spise eller drikke, er slappe og vanskelige å få kontakt med, skal lege oppsøkes. Dersom foreldre føler seg usikre skal man også kontakte lege. Terskelen for å oppsøke lege skal være lav, spesielt i første leveårene, sier hun.',
  '2018-11-18 16:44:33',
  'https://articles.familieklubben.no/sites/default/files/styles/foundation_large/public/field/image/477510166.jpg',
  'Foto: Shutterstock / NTB scanpix',
  2,
  1
),
(
  'Krever at politiet etterforsker mirakelpredikant: – Dette er svindel',
  'Etter VGs avsløringer om mirakelpredikant Svein-Magne Pedersens nødhjelp, mener bistandsbransjen at innsamlingene hans fremstår som svindel. – Politiet bør undersøke, sier strafferettsekspert. VG avslørte lørdag hvordan mirakelpredikanten Svein-Magne Pedersen har samlet inn penger fra nordmenn til nødhjelpsprosjekter som ikke eksisterer.

– Denne saken gjør meg forbannet. Dette er kynisk svindel, og Pedersen utnytter folks godhet i Norge, sier Petter Eide, som har jobbet med bistandsarbeid i 15 år. – Veldig alvorlig

– Dette er veldig alvorlig, fordi det ødelegger for mange mennesker som har et ønske om å bidra og gjøre noe godt.

Eide er mest bekymret for at predikantens praksis vil ødelegge tilliten til andre organisasjoner som driver med bistandsarbeid– Svein-Magne Pedersens virksomhet rammer andre organisasjoners muligheter til å samle inn til sine formål. Folk mister tillit til at pengene går dit det skal, sier han. – Hver gang det kommer ut saker som dette, er vi oppmerksom på at det i et totalinntrykk kan bidra til å svekke tilliten til bistandsorganisasjoner i Norge generelt, sier Toyomasu.

Eide, Toyomasu, og de andre ekspertene i denne saken, uttaler seg på bakgrunn av det som har kommet fram i VGs avsløringer.

Les hele avsløringen: Skryter av nødhjelp - dette fant VG
– Ført bak lyset og lurt

VGs kan dokumentere at Pedersen aldri har finansiert 24 barnehjem slik han har hevdet til sine givere, men to. På ett av disse bor det bare kuer. Det andre er ikke i drift som barnehjem – men brukes til å tilby et titalls barn leksehjelp.

Toyomasu synes det fremstår som svindel. – Hver gang det kommer ut saker som dette, er vi oppmerksom på at det i et totalinntrykk kan bidra til å svekke tilliten til bistandsorganisasjoner i Norge generelt, sier Toyomasu.

Eide, Toyomasu, og de andre ekspertene i denne saken, uttaler seg på bakgrunn av det som har kommet fram i VGs avsløringer.

Les hele avsløringen: Skryter av nødhjelp - dette fant VG
– Ført bak lyset og lurt

VGs kan dokumentere at Pedersen aldri har finansiert 24 barnehjem slik han har hevdet til sine givere, men to. På ett av disse bor det bare kuer. Det andre er ikke i drift som barnehjem – men brukes til å tilby et titalls barn leksehjelp.

Toyomasu synes det fremstår som svindel.',
  '2018-11-18 16:44:33',
  'https://smp.vgc.no/v2/images/b1023356-1b82-43d4-9d81-0104822a9d2e?fit=crop&h=667&w=1000&s=9dd331f397b6225ba83a96ace1b5ec380cf093ae',
  ' NØDHJELP: Svein Magne Pedersen har bedt sine tilhengere om støtte til det som skulle være 24 barnehjem i India. Foto: Paul Sigve Amundsen, VG',
  3,
  2
),
(
  'Dette betyr brexit-avtalen for Norge',
  'Kamp om oppmerksomheten både i EU og i Storbritannia, samt en ny måte å løse tvister og konflikter på. Det kan bli noen av konsekvensene for norske interesser med brexit-avtalen.Onsdag kveld var det klart at EU og Storbritannia har forhandlet seg fram til et utkast til avtale med detaljerte bestemmelser om skilsmissen. Forhandlingene om det framtidige forholdet mellom Storbritannia og EU starter imidlertid først når skilsmissen er et faktum.

I avtalen kommer det frem at Storbritannia vil ha en overgangsperiode på 21. måneder når de trekker seg ut av EU i april neste år. I denne overgangsperioden skal det aller meste i praksis fortsette som før, som om Storbritannia fortsatt var med i EU, men uten at britene får være med i beslutningsprosessene.

I denne perioden skal Norge forhandle om sitt fremtidige forhold til Storbritannia. – Det er usikkert hva dette vil bety for Norge, i og med at man ikke vet om Storbritannia kommer til å gå helt ut av EU, sier Kristian Steinnes professor ved NTNU.

Steinnes forteller at alle avtalene Norge har med Storbritannia, som et EU-medlem gjennom EØS-avtalen, brytes når Storbritannia eventuelt går ut av EU.

– Det er primært handelsavtaler, men også andre avtaler som utdanningsavtaler, sier professoren. EU-forsker og professor ved ARENA Senter for europaforskning ved Universitetet i Oslo og Universitetet i Agder, Jarle Trondal, poengterer at brexit-prosessen tar enormt mye ressurser og administrativ kapasitet både i Storbritannia og EU, og at Norge får hard konkurranse om oppmerksomheten begge steder.– Det er knapphet på oppmerksomhet i et overbelastet statsapparat i Storbritannia, og Norge kan få konkurranse i Brussel når Storbritannia får en EØS-liknende ordning. Norge kan risikere å få mindre innflytelse og hard konkurranse om deltakelse i styrer og råd i Brussel, mener Trondal:

– Norges utenforskap i EU gjelder ikke bare i saksbehandlingen fra dag til dag, men også ved at Norge ikke er med på forhandlingene. Derfor kan det komme overraskelser, mener professoren.
– Må ha avtalene på plass

Erik Oddvar Eriksen, direktør for Arena Senter for Europaforskning ved UiO peker spesielt på to ting som er viktig for Norge.

– Vi må ha noen som kan avgjøre tvister og konflikter dersom de oppstår, i Norge har vi allerede EFTA-domstolen og EUs avtaleverk som sikrer sømløs handel. Dette faller bort når Storbritannia ikke lenger er EU medlem, sier Eriksen til VG. – Norge må forhandle frem egne avtaler med Storbritannia, blant annet knyttet til handel og rettigheter, fortsetter han.

Eriksen forteller at det vil være problematisk dersom disse avtalene ikke er på plass når Storbritannia ikke lenger er en del av EU.

– Man må ha avtalene på plass, hvis ikke vet man ikke hva man skal gjøre dersom varer skal fraktes fra Norge til Storbritannia, og hvordan det skal klareres, sier Eriksen.

Professor Anna Nylund på det juridiske fakultetet ved Universitetet i Tromsø påpeker at for Norge og EØS sin del må man forhandle frem en avtale med Storbritannia som ligger tett opptil, eller er tilsvarende, den landet skal ha med EU etter at utmeldelsen er fullbyrdet. ',
  '2018-11-17 16:44:33',
  'https://smp.vgc.no/v2/images/157a4739-64d3-4f8a-ba93-45830a99917b?fit=crop&h=700&w=1000&s=ce7e8d4482308860bea8d342141f24d1fd91238e',
  'NYE AVTALER: Statsministerne Erna Solberg og Theresa May må igjen snakke sammen og lage nye avtaler dersom Storbritannia bryter helt med EU. Bildet er tatt da Solberg besøkte May i Downing Street tidligere i år. Foto: Alastair Grant / TT NYHETSBYRÅN',
  5,
  1
),
(
  'Norsk hoppskuffelse: Forfang beste nordmann på tiendeplass',
  'På en dårlig norsk dag i bakken var Johann André Forfang (23) best av de norske. Daniel-André Tande (24) kvalifiserte seg ikke engang til finaleomgangen. Landslagstrener Alexander Stöckl har nok ikke fått de svarene han ønsket etter verdenscupåpningen i polske Wisla. Lørdag ble lagkonkurransen spolert av en diskvalifikasjon og søndag var de norske gutta sjanseløse mot de aller beste. Forfang endte på tiendeplass med hopp på 122,5 og 128 meter. Russiske Jevgenij Klimov (24) ledet etter 1.-omgang, og da annenmann Kamil Stoch ikke holdt nervene i sjakk i finaleomgangen, kunne 24-åringen feire Russlands aller første individuelle verdenscupseier etter et hopp på 131,5 meter. 1.-omgang i Wisla ble en eneste stor skuffelse for de norske hopperne. Hverken Daniel-André Tande eller Andreas Stjernen kvalifiserte seg til 2.-omgang med hopp på henholdsvis 113 og 119 meter. ',
  '2018-11-16 16:44:33',
  'https://smp.vgc.no/v2/images/cea66577-d2a7-4b6c-80c7-98e302c3e150?fit=crop&h=667&w=1000&s=3cf336e04e50c108269b727eb50c8abfe83b2a2b',
  'TØFFE TIDER: Daniel-André Tande har vært betenkt under forberedelsene til hoppsesongen. Her fra Raw Air i Granåsen i mars. Foto: Wold, Ole Martin / NTB scanpix',
  1,
  1
),
(
  'Her kjemper kvinner for å kunne ta lovlig abort',
  'ISTANBUL (VG) Tyrkias president Erdogan mener abort er drap. Loven gir kvinner rett til fri abort, men sykehusene avviser dem.Mens det i Norge pågår en stor debatt om endring av abortloven, finnes det flere eksempler fra utlandet som demonstrerer hvordan ulik praksis rammer kvinner i en sårbar situasjon.I Tyrkia er det lovbestemt fri abort inntil uke ti, altså to uker kortere enn i Norge. I teorien skal kvinner i Tyrkia kunne oppsøke offentlige sykehus og få gjennomført en trygg abort, uten at noen spørsmål blir stilt.Men i praksis er virkeligheten en ganske annen.Da Busra (24) fra Istanbul oppdaget at hun var gravid for to år siden, visste hun at hun ville ta abort. Men hun visste ikke hvordan.– Det var uaktuelt for meg å fortelle det til foreldrene mine, de er svært konservative. Hvis jeg hadde gått på et offentlig sykehus, ville jeg automatisk ha blitt registrert på min fars helseforsikring, og han ville kunne ha oppdaget aborten, forteller Busra.I Tyrkia deler alle døtre sin fars helseforsikring inntil de selv blir gift, en form for «positiv diskriminering».Etter å ha avskrevet en av Istanbuls mange «undergrunnsklinikker» som tilbyr abort i det skjulte uten ID-registrering, fikk Busra time ved et privatsykehus. Hun var seks uker på vei, og fikk gjennomført en kirurgisk abort. Hun betalte 1000 tyrkiske lira (1600 kroner), og ingen i familien fikk noen gang vite noe. Av den grunn vil hun heller ikke stå frem med etternavn og bilde.',
  '2018-11-14 16:44:33',
  'https://smp.vgc.no/v2/images/9a8f38ca-e1ef-4db7-95e3-e176aa6ee54e?fit=crop&h=670&w=1000&s=a619025c6cced4731a7b57bbf65145b1e2e66af0',
  'PRO-ABORT: Feminist, aktivist og lege Gülsüm Kav jobber med en rekke kvinnespørsmål, deriblant abort. Her fotografert i Istanbul. Foto: Ozge Sebzeci',
  2,
  1
),
(
  'I fjor brukte norske shoppere 2,5 millioner kroner i minuttet på Black Friday. I år blir det enda mer.',
  'Så er den her igjen. Den importerte shoppingdagen vi først omtalte i 2010, og som de siste årene har vokst seg til å bli Norges aller største shoppingfenomen.

På Black Friday i fjor brukte nordmenn drøye 3,6 milliarder kroner i norske butikker på fredagen alene. Eller sagt på en annen måte: Vi brukte 2,5 millioner kroner i minuttet under fjorårets Black Friday.

Det var rekordmye, og i år forventes rekorden å øke til 3,9 milliarder, ifølge handelsorganisasjonen Virke.

– Det blir tidenes Black Friday, sier Elkjøp.

– Utspekulert påfunn, raser Fremtiden i våre hender.

De første årene var tilbudsfesten forbeholdt bare én dag, fredagen etter Thanksgiving i USA, men har nå blitt et fenomen som strekker seg over hele uken. Her i Norge starter de fleste butikkene tilbudene sine på førstkommende mandag, 19. november. Da vil flere aktører har såkalte «dagstilbud» som skal lokke deg til å fiske fram kredittkortet, og finalen kommer på selveste Black Friday, 23. november.

Butikkene, pristjenester og analytikere spår at nordmenn vil bruke mer på årets «Black Week»-uke enn fjoråret.En stor aktør som genuint har omfavnet Black Friday-konseptet er Elkjøp. De starter med tilbud allerede på mandag.

– Vi har også i år valgt å spre tilbudene utover uken, da vi hadde god erfaring med det fra fjorårets Black Friday, forteller Madeleine Schøyen Bergly, som er kommunikasjonssjef i Elkjøp Norge.

Dette skal ha sørget for en vekst på solide 75 prosent sammenlignet med året før.

– Vi ønsker å minske kø, og sørge for at kunder har god tid på å sikre seg tilbudene på produktene som står øverst på ønskelisten, samtidig som vi bidrar til en mer stressfri handel, forteller Bergly.

– Vi legger opp til at dette skal bli tidenes Black Friday.Mens mange sikkert har planer om å la kredittkortet gå varmt til uka, er ikke alle like begeistret for Black Friday-konseptet. Herunder finner vi Framtiden i våre hender, som blant annet abeider for rettferdighet og et grønt forbruk.

– Black Friday er ene og alene et utspekulert påfunn fra handelsstanden for å øke egen fortjeneste og for å lure folk til å kjøpe ting de ofte ikke trenger, mener Anja Bakken Riise, som leder organisasjonen.

Mye tyder på at hun har et poeng.

Ifølge en nylig undersøkelse gjort av Opinion Matters for markedsplassen Gumtree.com, føler nemlig tre av fem shoppere seg presset til å kjøpe noe på Black Friday.

Så mange som en tredjedel har derfor endt opp med å kjøpe noe de strengt tatt har fra før, ganske enkelt fordi det er billig.

Eller fordi vi tror det er billig, minner Framtiden i våre hender oss på.

– Vi vet at mange av tilbudene ofte ikke er spesielt gode. Tidligere er det blitt avslørt at store forhandlere faktisk har skrudd opp førprisene for så å lokke med lave priser på Black Friday. Det er fort gjort at forbrukerne blir ført bak lyset på en slik dag, sier Anja Bakken Riise.

På dette punktet får hun medhold av Are Vittersø, daglig leder i Prisjakt, som har god oversikt over hva slags produkter som er på tilbud, og hvor mye prisene varierer.',
  '2018-11-08 16:44:33',
  'https://tek.vgc.no/2365/2365240/tb893371.1250x703.jpg',
  ' Elkjøp Megastore Ullevål, her i 2016. (Foto: Frode Hansen, VG) ',
  5,
  1
),
(
  'Regjeringspartiene med nytt tilbud til KrF',
  'Halvannen time på overtid la regjeringspartiene søndag fram et nytt tilbud til KrF i forhandlingene på Stortinget om neste års statsbudsjett. Halvannen time på overtid la regjeringspartiene søndag fram et nytt tilbud til KrF i forhandlingene på Stortinget om neste års statsbudsjett. – Nå har vi kommet med et nytt tilbud, så da blir det nok noen timers arbeid, sa Høyres finanspolitiske talsmann og finanskomiteens leder Henrik Asheim på vei inn.Venstres finanspolitiske talsmann Abid Raja la an en optimistisk tone, mens Fremskrittspartiets finanspolitiske talsmann Helge André Njåstad varslet at det ikke er veldig mye mer å gå på. – Jeg vil tro at KrF vil mene at dette helt ferske tilbudet er betydelig bedre enn sist vi så dem. Vi nærmer oss slutten og forhåpentlig når vi fristen tirsdag, sa Raja. – Vi har kommet KrF i møte i hvert eneste møte. Nå nærmer vi oss streken, understreket Njåstad. ',
  '2018-10-18 16:44:33',
  'https://smp.vgc.no/v2/images/3fb8ee9b-8e99-4847-bb32-ff78ca65928a?fit=crop&h=667&w=1000&s=5587506a2cfb28ad3107a5e46dc1eea31cf66b5b',
  'NYTT TILBUD: KrF-nestleder og finanspolitisk talsmann Kjell Ingolf Ropstad (til høyre) fikk søndag et nytt tilbud fra regjeringspartienes finanspolitikere Henrik Asheim (H), Abid Raja (V) og Helge André Njåstad (Frp) i forhandlingene om neste års statsbudsjett. Foto: Fredrik Hagen / NTB scanpix ',
  5,
  1
),
(
  'Mann siktet for drapsforsøk etter knivstikking på Blå',
  'En mann er siktet for drapsforsøk etter knivstikkingen på Blå. Mannen er ikke pågrepet. Like før klokken 02.30 rykket politiet ut etter melding om et slagsmål inne på det populære utestedet Blå på Grünerløkka. Da de kom frem viste det seg at minst to personer var knivstukket. De to ble sendt til sykehus med stikkskader.

Politiinspektør Grete Lien Metlid opplyser til VG at en person er siktet for drapsforsøk. – Mannen er ikke pågrepet, og politiet jobber nå med å få tak i ham, sier hun.

Politiinspektøren vil ikke kommentere årsaken til at politiet har tatt ut siktelsen.

– Vi kan ikke si noe om opptakten eller motivet nå, men vi har opplysninger som gjør at vi har tatt ut siktelse og aktivt søker etter en konkret, navngitt person, sier hun.

– Vi ser at det er folk involvert som vi har kjennskap til, og som vi kan knytte til et kriminelt miljø.

Politikilder opplyser til TV 2 at de tre involverte har tilknytting til den kriminelle gjengen Young Bloods fra Holmlia i Oslo. Dette bekrefter Metlid overfor kanalen.Metlid opplyser til VG at en annen mann tidligere i dag ble pågrepet og siktet for drapsforsøk i saken, men at vedkommende nå er løslatt.

Ifølge Metlid er tilstanden til begge de to fornærmede stabile. Politiet har gjennom natten jaktet på en eller flere gjerningspersoner.

– Det har vært en hektisk natt, med mange opplysninger og mange folk involvert, sier Metlid.

Utrykningsleder i kriminalvakta Øyvind Myhr opplyste til VG klokken 09.50 at det er to menn i 20-årene som er knivstukket. Politiet har fått opplysninger om at en tredje person kan være skadet, men denne personen har de ikke kontroll på.

– Vi har ikke fått noe mer informasjon om dette, og vi har således ikke funnet noen, sa vaktleder for krimvakten i Oslo, Guro Sandnes, tidligere i dag. ',
  '2018-10-28 16:44:33',
  'https://smp.vgc.no/v2/images/970dc1de-7255-4c4b-865c-a7f53a818875?fit=crop&h=667&w=1000&s=671510dc50a82a76b789e95793ec86221b12cadf',
  'POPULÆRT UTESTED: Politiet jobber natt til søndag på stedet. Foto: Ruud, Vidar / NTB scanpix',
  5,
  1
),
(
  'Ferrante-regissør: – Som å jobbe med et spøkelse',
  'CASERTA (VG) Regissør Saviero Costanzo (43) jobbet tett med den mystiske Elena Ferrante om HBO-serien «My brilliant friend» – basert på hennes enorme boksuksess – men han møtte henne aldri, og beskriver det som et rent mareritt.– Det var som å jobbe med et spøkelse, sier Costanzo til VG da vi møtte ham på settet i Caserta utenfor Napoli i sommer.Den påkostede TV-serien har premiere på HBO mandag og er basert på den første boken i Napoli-kvartetten skrevet av den italienske forfatteren som ingen helt vet hvem er, som ikke vil at noen skal vite hvem hun er - og som skriver under pseudonymet Elena FerranteDen italienske regissøren, som tidligere har regissert flere langfilmer, beskriver det å jobbe med en anonym person som et mareritt.
  – Det er forferdelig. Jeg har drømt mye om natten om personer uten ansikt. Ferrante er en krevende person å jobbe med, og hun kan få meg til å føle meg svak. Men samtidig har vi hatt et klart og tydelig forhold. Hun sier ikke så mye, men har veldig tydelige ideer og har gitt gode råd underveis, sier Costanzo. ',
  '  2018-10-18 15:44:33',
  'https://smp.vgc.no/v2/images/7b6135a6-06c3-4532-b4d9-d795ee91ded4?fit=crop&h=754&w=1000&s=dfd82d43a20c22197cce01e2d27010db38ce3a7c',
  'VET IKKE HVEM FERRANTE ER: Regissør Saviero Costanzo med Lila (t.v) og Elena under innspillingen av HBO-serien «My brilliant friend». Han sier til VG at han ikke aner hvem Ferrante er, selv om de to har skrevet hundrevis av e-poster til hverandre. Foto: HBO Nordic',
  3,
  2
),
(
  '
  Han er mannen bak sjakkcomputeren alle snakker om
  ',
  '
  LONDON (VG) Mannen bak «Sesse»-analysen, Steinar H. Gunderson, mener at Team Carlsen er «gode til å holde på hemmelighetene sine». I motsetning til Caruana-teamet, smiler han på plass i London under VM-matchen mellom verdensmester Carlsen (27) og utfordrer Caruana (26) (3–3 etter seks partier).
  – Jeg tror det var en direkte feil at videoen ble lagt ut. Det er kanskje ikke kjempeviktig i den store sammenhengen, men det er en «big deal», fastslår dataeksperten som sjakk-kommentatorene hele tiden snakker om i VM-sendingene.
  Når de skal virkelig dypt i materien, går de til Gundersons «Sesse».
  – Publiseringen av videoen var noe som aldri skulle ha skjedd. Men jeg skjønner at det kan skje. Det kan skli gjennom.Hverken Fabiano Caruana eller managementet hans har kommentert tirsdagens videoskandale. Det ble lagt ut en video fra Caruanas sjakk-klubb i St. Louis. De hadde fulgt hans treningsleir før VM. Men – i motsetning til en tilsvarende video fra Magnus Carlsens treningsleir på Kragerø Resort – var hverken mennesker eller analyser sladdet. – Jeg føler at Team Carlsen er gode til å holde på sine hemmeligheter. Det er viktig å holde kortene tett til brystet i en slik match, og det klarer de, sier Steinar H. Gunderson.
  ',
  '
  2018-10-18 16:44:33
  ',
  '
  https://smp.vgc.no/v2/images/22d03a0c-0d8c-4937-97a8-b26a0def6cc7?fit=crop&h=750&w=1000&s=90bbca9a83bf2177317662c926a33837164b4921
  ',
  '
  SJAKK-ANALYTIKER: Steinar H. Gunderson. Foto: Ole Kristian Strøm
  ',
  1,
  2
),
(
  'Lady Gaga er forlovet',
  'Stefani Joanne Angelina Germanotta (32), som er best kjent under artistnavnet Lady Gaga, delte nyheten fra talerstolen under Elle’s 25-årsjubileum mandag kveld. A Star is Born-stjernen ble hedret under Women in Hollywood-prisutdelingen, skriver People.
    Germanotta og den 17 år eldre kjæresten Christian Carino ble et par i februar i fjor. Det har lenge vært spekulert i at paret er forlovet, men dette er første gangen en av dem selv har uttalt seg om det.
    Artisten har tidligere vært forlovet – med skuespilleren Taylor Kinney. Forholdet, som varte i fem år, ble imidlertid slutt før de rakk å gifte seg.
    I talen delte hun ikke bare den gledelige nyheten. Hun åpnet også opp om seksuelt overgrep da hun snakket om hvilket antrekk hun skulle gå i under arrangementet.
    Hun fortalte at hun følte seg kvalm etter å ha prøvd kjole på kjole, og at hun til slutt falt på en stor dress.
    – Som en overlever etter seksuelt overgrep av noen i underholdningsbransjen, som en kvinne som fortsatt ikke er modig nok til å si hans navn, som en kvinne som lever med kroniske smerter, og som en kvinne som var betinget i en svært ung alder til å lytte til hva menn fortalte meg å gjøre,
    har jeg i dag valgt å ta makten tilbake. Idag velger jeg å gå i bukser, sa hun.
    – Å motstå standarden på å kle seg for imponere. Å bruke det som virkelig betyr noe – stemmen min.',
  '2018-10-16 16:44:33',
  'https://smp.vgc.no/v2/images/e52d9111-9011-4e66-9d6e-934b18403bd3?fit=crop&h=1393&w=1900&s=a264d6abfff103632f77ce7752fdd0cb7c4f2520',
  'FORLOVET: Lady Gaga avslørte under Elle’s 25-årsjubileum at hun og kjæresten Christian Carino er forlovet.',
  3,
  1
),

(
'Influencere etterlyser tydeliggjøring av etiske retningslinjer mot kroppspress',
'– Jeg stiller meg bak de nye etiske retningslinjene mot kroppspress, men det må være litt mer presist, sier Kristin Gjelsvik.
Barne- og likestillingsminister Linda Hofstad Helleland (H) og folkehelseminister Åse Michaelsen (Frp)
inviterte tirsdag ettermiddag til et diskusjonsmøte med influencere
hvor forslaget til etiske retningslinjer om kroppspress sto på agendaen. Statsrådene ønsker å vite hva influencere, bloggnettverk, annonsører
og ikke minst de unge selv tenker om retningslinjene og hvordan de skal få bransjen til å følge de nye reglene.
– Jeg håper dere som er her, som har så mye makt, vil bidra til disse etiske retningslinjene, åpnet Hofstad Helleland diskusjonsmøtet.
Bakgrunn: Nå får bloggere etiske retningslinjer mot kroppspress
Hofstad Helleland har tidligere uttrykt at hun ønsker en dialog med bransjen selv om det nye regelverket, men da hun inviterte til en
samarbeidsmøte i sommer var det labert oppmøte blant influencerne.
Det stoppet imidlertid ikke regjeringen, og nylig la de fram forslaget på de nye etiske retningslinjene som skal motvirke kroppspress
på sosiale medier.
Retningslinjene gjør det blant annet ulovlig å promotere kosmetiske inngrep eller injeksjoner som gir en varig eller
langvarig endring av utseendet, for eksempel silikon og fillers. Det nye regelverket foreslår også at det skal være ulovlig å
markedsføre kosttilskudd og tilsvarende produkter som er ment å gi vektreduksjon, større muskler eller på annen måte fremme et bestemt kroppsideal.
Tirsdag ettermiddag var blant andre Kristin Gjelsvik, Anniken Jørgensen, Martine Halvorsen, Sofie Nilsen, Cornelia Grimsmo,
Anne Brith Davidsen og Vita og Wanda Mashadi til stede for å diskutere de nye retningslinjene.
– Jeg stiller meg bak de nye etiske retningslinjene mot kroppspress, men de må tydeliggjøres ytterligere, sier Gjelsvik til MinMote,
Hun får støtte av flere av de andre influencerne.
– Ting må være litt tydeligere med tanke på hva som er lov og ikke når det kommer til for eksempel kosttilskudd.
Gjelder det Omega 3 eller er det slankepiller det er snakk om, spør «Treningsfrue» Camilla Aastorp Andersen.
Hofstad Helleland mener at det er opp til bransjen selv å gå i detaljnivå.
– Det er noen tydeliggjøring som ønskes, men det føler jeg ikke er opp til oss politikere.
Vi er ikke de som skal sitte med detaljstyringen, fordi det her noe som bransjen selv kjenner bedre til.
Men vi skal se på om det er noe vi skal tydeliggjøre før vi sender det fra oss, sier Helleland.',
'2018-10-02 12:22:23',
'https://1.vgc.no/drpublish/images/article/2018/10/16/24467800/1/normal-big_20/IMG_0846.jpg',
'INFLUENCER: Kristin Gjelvik ønsker å få på plass de nye retningslinjene så fort som mulig. ',
2,
2
),

(
  'Carlsen med lånt kortbukse: – Det er et diplomati',
  'Magnus Carlsen (27) orket ikke tanken på en ny dag i dress og var tilbake i kortbukse i tirsdagens Europacup-spill for Vålerenga. Nå må han ha 1,5 av 2 poeng i resten av turneringen for å beholde plassen som verdensener.
  – Magnus lånte ei kortbukse av lagkamerater Kjetil Lie. Men den er lengre enn den han brukte de to første rundene. Den går under kneet. Jeg har ikke hørt noen reaksjoner på den, sier Vålerengas lagleder Dag Danielsen til VG.
  Verdensmesteren fikk påpakning før mandagens fjerde runde i Hellas: Han var ikke pen nok i tøyet ut fra reglene.
  Carlsen stilte derfor i dress mandag, men med temperaturen i spillelokalet og den dårlige luften, valgte han å bytte til en shorts som gikk nedenfor knærne til tirsdagens parti mot verdenstreer Shakhriyar Mamedyarov.
  – Det er et diplomati med arrangøren og det europeiske forbundet. Det er begrenset hva vi kan få tak i av klær her, men jeg har forklart for arrangøren
  og forbundet at det blir for varmt for Magnus med dress, så han ønsker å bruke shorts, forklarer Dag Danielsen.',
  '2018-03-02 17:23:56',
  'https://smp.vgc.no/v2/images/b9a334ae-3ac0-4454-b5f0-ad36f29061a2?fit=crop&h=1267&w=1900&s=3898b0aeca7e206010a3e2deadb4cbfa83886367',
  'HETT: Magnus Carlsen sliter med at det er varmt i spillelokalet i Hellas under Europacupen ',
  1,
  1
),

(
  'Søreide tok ikke opp menneskerettigheter med Kinas president',
  'Utenriksminister Ine Eriksen Søreide (H) tok opp menneskerettigheter i politiske samtaler i Kina, men ikke under selve møtet med president Xi Jinping.
  Utenriksministeren sier at hun tok opp temaet «under de politiske samtalene som fulgte» tirsdag kveld – altså under banketten.
  – Jeg tok det opp i mine politiske møter, som fulgte rett i etterkant. Nettopp fordi møtet mellom presidenten og kongen ikke er det beste formatet å ta disse spørsmålene i, sier Søreide til NTB.
  Under statsbesøk foregår møtet formelt sett mellom statsoverhodene. Dermed er det kong Harald som samtaler med den kinesiske presidenten. Han har også muligheten til å gi ordet videre til andre – deriblant statsrådene.
  Søreide bekrefter at hun fikk ordet under møtet med presidenten. Likevel tok hun ikke opp menneskerettighetsspørsmål.
  – Akkurat det formatet, i møtet mellom de to statslederne, er ikke det best egnede formatet for å gjøre det. Det er nettopp derfor jeg som utenriksminister, og ansvarlig for utenrikspolitikken, gjør det vi alltid gjør. Nemlig å ta dette i de politiske samtalene som fulgte rett etter, sier Søreide.
  – Ikke være besatt av menneskerettigheter
  Etter kongens møte med presidenten fikk den kinesiske UD-toppen Liu Weimin spørsmål fra norsk presse om menneskerettigheter.',
  '2018-05-23 05:23:56',
  'https://smp.vgc.no/v2/images/94c00649-e5b5-4738-9bfd-28d22f42f9c8?fit=crop&h=1267&w=1900&s=94725fe75485debf1a5e8cd4ce2fcc9c687afd24',
  'En samarbeidsavtale om internasjonal innovasjon ble skrevet under av Innovasjon Norge-sjef Anita Krohn Traaseth og Gao Yan, Kinas sjef for internasjonal handel. ',
  5,
  1
),

(
  'Drapsforsøk med musegift opp for Høyesterett',
  'Kvinnen som er dømt for gjentatte drapsforsøk ved å fôre ektemannen med musegift, får anken sin opp i Høyesterett.
  Den 31 år gamle brasilianske kvinnen er dømt for å ha lurt sin 12 år eldre ektemann til å spise musegift gjentatte ganger. Borgarting lagmannsrett falt ned på 11 års fengsel for drapsforsøk. Retten fant det skjerpende at hun i lengre tid hadde planlagt drapet, og at hun forsøkte å kamuflere det som selvmord.
  Høyesteretts ankeutvalg vil ikke slippe igjennom kvinnens anke over straffeutmålingen, men tillater at lovanvendelsesanken tas opp til behandling. Det betyr at domstolen skal vurdere om lagmannsretten har forstått loven korrekt da juryen fant kvinnen skyldig og retten til slutt avsa dom.
  – Trakk seg fra forsøk
  Kvinnens forsvarer mener hun ikke skal dømmes for det siste av sju drapsforsøk, fordi hun ved dette tilfellet tilkalte hjelp fra AMK framfor å fullbyrde drapsforsøket.
  – Hun ringte alarmsentralen ved to ganger i denne situasjonen og sørget for at han fikk help. Hun trakk seg fra handlingen, sier advokat Hilde Marie Ims til NTB.
  Den brasilianske kvinnen begynte våren 2016 å vurdere muligheten for å ta ektemannen av dage, ifølge dommen. Forholdet dem imellom hadde surnet flere år tidligere, og tiltalte hadde i årene fram til 2016 hatt flere forhold til andre menn.
  Musegift i vaniljesaus
  Hun vedgikk i retten at hun serverte mannen musegift fire ganger ved å blande den i en gryterett, en frokostblanding, en smoothie og i vaniljesaus. Retten mente at hun kun innrømmet disse forholdene fordi andre bevis gjorde dem umulig å bortforklare.
  Et flertall av dommerne fant bevismessig grunnlag for at hun lurte mannen til å spise musegift sju ganger på sensommeren og høsten 2016.',
  '2018-09-24 21:45:32',
  'https://smp.vgc.no/v2/images/29a5d7d5-ece1-410c-9c9c-19448ce5d589?fit=crop&h=1386&w=1900&s=a9130d5f9b6f8ce451831887cc3dfee3b8e5fc0b',
  'Advokat Hilde Marie Ims forsvarer den brasilianske kvinnen som er dømt for å ha forsøkt å ta livet av sin mann med musegift. ',
  4,
  1
),

(
  'Nå vet alle låskoden til Kanye West',
  'Nei, 000000 er ikke en særlig trygg kode.
  Kanye West var på besøk hos den amerikanske presidenten i det Hvite Hus i går. Der låste han opp iPhonen sin for å vise
  Donald Trump et par nye forslag til et alternativt fly han mener Apple burde bygge for presidenten.
  I stedet for å låse opp ansiktet valgte han å slå inn sin sekssifrede låskode, som nå har blitt foreviget på film: 0 0 0 0 0 0.
  Mobilen din vil til og med påminne deg nøyaktig hvor mye innhold som du prøver å sikre med denne ene koden,
  for å poengtere hvor viktig det er at koden er trygg.
  Ettersom han bruker en nyere iPhone med ansiktsopplåsning, trolig iPhone Xs, burde han naturligvis ha brukt ansiktet sitt
  for å låse opp mobilen, og unngått hele problemet.
  Ville vise Trump «iPlane 1»
  Grunnen til at Kanye West tok frem mobilen sin var for å vise det han kalte iPlane 1, et konsept for et hydrogendrevet
  fly han mente Apple burde vurdere å lage.
  – Denne burde du fly rundt i, sa han til Trump mens han viste frem mobilen med bildene.
  Trump svarte med å stille et spøkende spørsmål til de rundt seg om han kunne bytte ut AirForce One med Kanyes forslag.',
  '2018-08-24 23:12:34',
  'https://tek.vgc.no/2351/2351855/lockcode.1195x672.jpg',
  '000000 er ikke en særlig trygg låskode på iPhonen din, uansett om du er kjendis eller ei.',
  3,
  1
);
