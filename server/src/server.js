// @flow

import express from "express";
import path from "path";
//var express = require("express");
var mysql = require("mysql");
var bodyParser = require("body-parser");
let app = express();
var apiRoutes = express.Router();
app.use(bodyParser.json()); // for å tolke JSON
const public_path = path.join(__dirname,'/../../client/public');
app.use(express.static(public_path));
app.use(express.json());
const NyhetsDao = require("./dao/nyhetsdao.js");



//Kobler til database
var pool = mysql.createPool({
  connectionLimit: 2,
  host: "mysql.stud.iie.ntnu.no",
  user: "elisamop",
  password: "ktWkDPy1",
  database: "elisamop",
  debug: false
});

type Request = express$Request;
type Response = express$Response;


let nyhetsDao = new NyhetsDao(pool);

app.get("/nyheter", (req : Request, res : Response) => {
  console.log("/nyheter fikk request fra klient");
  nyhetsDao.getAll((status, data) => {
    res.status(status);
    res.json(data);
  });
});

app.get("/nyheter/kategori/:kategorinavn", (req : Request, res : Response) => {
  console.log("nyheter/kategori/:kategori_id fikk request fra klient");
  nyhetsDao.getKategoryNews(req.params.kategorinavn, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

app.get("/nyheter/:id", (req : Request, res : Response) => {
  console.log("/nyheter/:id fikk request fra klient");
  nyhetsDao.getOne(req.params.id, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

//DENNE BRUKES IKKE
app.get("/nyheter/livefeed", (req : Request, res : Response) => {
  console.log("/nyheter/livefeed fikk request fra klient");
  nyhetsDao.getLivefeedNews((status, data) => {
    res.status(status);
    res.json(data);
  });
});

app.post("/nyheter", (req : Request, res : Response) => {
  console.log("Fikk create POST-request fra klienten");
  nyhetsDao.createOne(req.body, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

app.put("/nyheter/:id", (req : Request, res : Response) => { //eller man kan bruke put istedenfor post
  console.log("Fikk update POST-request fra klient");
  nyhetsDao.updateOne(req.body, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

app.delete("/nyheter/:id", (req : Request, res : Response) => {
  console.log("Fikk delete-request fra klient");
  nyhetsDao.deleteOne(req.params.id, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

var server = app.listen(3000);
