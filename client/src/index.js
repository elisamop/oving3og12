// @flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { HashRouter, Route, NavLink } from 'react-router-dom';
import { Alert, NavBar, NavBootItem, CardEn, CardMange, CardForm } from './widgets';
import { nyhetStore } from './stores';
import Marquee from "react-smooth-marquee";

// Reload application when not in production environment
if (process.env.NODE_ENV !== 'production') {
  let script = document.createElement('script');
  script.src = '/reload/reload.js';
  if (document.body) document.body.appendChild(script);
}

import createHashHistory from 'history/createHashHistory';
const history = createHashHistory(); // Use history.push(...) to programmatically change path, for instance after successfully saving a student

let current = new Date();
let currentDate = current.getFullYear() + "-" + current.getMonth() + "-" + current.getDate();
let skaloppdatere : boolean = true;

class Menu extends Component {
  render() {
    return (
      <NavBar tittel='Hallo Verden Nyheter'>
        <NavBootItem linker='/nyheter/kategori/Sport'>Sport
        </NavBootItem>
        <NavBootItem linker='/nyheter/kategori/Helse'>Helse
        </NavBootItem>
        <NavBootItem linker='/nyheter/kategori/Kjendisnytt'>Kjendisnytt
        </NavBootItem>
        <NavBootItem linker='/nyheter/kategori/Miljø'>Miljø
        </NavBootItem>
        <NavBootItem linker='/nyheter/kategori/Samfunn'>Samfunn
        </NavBootItem>
        <NavBootItem linker='/nyheter'>Alle nyheter
        </NavBootItem>
        <NavBootItem linker='/create'>Opprett nyhet
        </NavBootItem>
      </NavBar>
    );
  }
}

class Livefeed extends Component {
  render() {
    return (
      <div className="container-fluid" id="livefeed">
      <Marquee>
        {
          nyhetStore.nyheter.filter(nyhet => //Filtrerer nyhetarray til livefeeden
          (
          (parseInt(currentDate.slice(0,4)) == parseInt(nyhet.tidspunkt.slice(0,4))) //Årstall må være lik nåværende
          &&
          (parseInt(currentDate.slice(5,7))+1 == parseInt(nyhet.tidspunkt.slice(5,7)))//Måned må være lik nåværende
          &&
          (((parseInt(currentDate.slice(8,11)) - parseInt(nyhet.tidspunkt.slice(8,11))) == 1) //artikkelens dato må være lik nåværende
          ||
          ((parseInt(currentDate.slice(8,11)) - parseInt(nyhet.tidspunkt.slice(8,11))) == 0)) //eller 1 dag siden
          &&
          nyhet.viktighet == 1 //viktighet må være viktig for å komme på livefeeden
          ))
          .map(nyhet => (
          <span className="livefeed" key={nyhet.id}>
            <NavLink id="livefeedlink" activeStyle={{ color: 'darkblue' }} exact to={'/nyheter/' + nyhet.id}>
              {nyhet.overskrift + "\t" + nyhet.tidspunkt}
            </NavLink>{'  '}
          </span>
        )).slice(0,3)

      }
      </Marquee>
      </div>
      );
   }

  mounted() {
    skaloppdatere = true;
    this.oppdaterLivefeed();
  }

  oppdaterLivefeed() {
    if(skaloppdatere){
      nyhetStore.getNyheter()
      .then( nyheter => {
        setTimeout( () => { this.oppdaterLivefeed(); }, 10000);
      })
      .catch((error: Error) => Alert.danger(error.message));
    }
  }
  componentWillUnmount(){
    skaloppdatere=false;
  }


}

class Home extends Component {
  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-sm-1"></div>
            <div className="col-sm-10">
              <div className="card-columns">
                {nyhetStore.nyheter.map(nyhet => (
                  <div key={nyhet.id}>
                    {console.log("Currentnyhet id når man velger nyhet" , JSON.stringify(nyhet))}
                    <CardMange
                      idlink={'/nyheter/' + nyhet.id}
                      overskrift={nyhet.overskrift}
                      bildeurl={nyhet.bildeurl}
                      bildeteks={nyhet.bildetekst}
                      tidspunkt={nyhet.tidspunkt}>
                    </CardMange>
                  </div>
                )).slice(0,15)}
              </div>
            </div>
          <div className="col-sm-1"></div>
        </div>
      </div>
    );
  }

  mounted() {
    nyhetStore.getNyheter().catch((error: Error) => Alert.danger(error.message));
  }
}

class NyhetList extends Component {
  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-sm-1"></div>
            <div className="col-sm-10">
              <div className="card-columns">
                {nyhetStore.nyheter.map(nyhet => (
                  <div key={nyhet.id}>
                    {console.log("Currentnyhet id når man velger nyhet" , JSON.stringify(nyhet))}
                    <CardMange
                      idlink={'/nyheter/' + nyhet.id}
                      overskrift={nyhet.overskrift}
                      bildeurl={nyhet.bildeurl}
                      bildeteks={nyhet.bildetekst}
                      tidspunkt={nyhet.tidspunkt}>
                    </CardMange>
                  </div>
                ))}
              </div>
            </div>
          <div className="col-sm-1"></div>
        </div>
      </div>
    );
  }

  mounted() {
    nyhetStore.getNyheter().catch((error: Error) => Alert.danger(error.message));
  }
}

class NyhetDetails extends Component<{ match: { params: { id: number } } }> {
  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-sm-2"></div>
            <div className="col-sm-8">
              {console.log("forsøker å skrive ut innholdet: ", JSON.stringify(nyhetStore.currentNyhet))}
              <CardEn
                overskrift={nyhetStore.currentNyhet.overskrift}
                bildeurl={nyhetStore.currentNyhet.bildeurl}
                bildeteks={nyhetStore.currentNyhet.bildetekst}
                tidspunkt={nyhetStore.currentNyhet.tidspunkt}
                innhold={nyhetStore.currentNyhet.innhold}
                kategori= {nyhetStore.currentNyhet.kategorinavn}>
              </CardEn>
              {console.log("Currentnyhet når man velger nyhet " , nyhetStore.currentNyhet)}
              <NavLink id="endrebtn" className="btn btn-primary" role="button" activeStyle={{ color: 'darkblue' }} to={'/nyheter/' + nyhetStore.currentNyhet.id + '/edit'}>
                Endre artikkel
              </NavLink>
              <NavLink id="slettbtn" className="btn btn-dark" role="button" activeStyle={{ color: 'darkblue' }} to={'/nyheter/' + nyhetStore.currentNyhet.id + '/delete'}>
                Slett artikkel
              </NavLink>
          </div>
        <div className="col-sm-2"></div>
      </div>
    </div>
    );
  }

  mounted() {
    nyhetStore.getNyhet(this.props.match.params.id).catch((error: Error) => Alert.danger(error.message));
  }
}

export class NyhetEdit extends Component<{ match: { params: { id: number } } }> {
  render() {
    return (
      <CardForm overskrift='Endre nyhetsartikkelen'>
        <div className="form-group">
          <label for="overskrift">Overskrift</label>
          <input
            className="form-control"
            type="text"
            id="overskrift"
            required
            value={nyhetStore.currentNyhet.overskrift}
            onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
              (nyhetStore.currentNyhet.overskrift = event.target.value)
            }
          />
        </div>
        <div className="form-group">
          <label for="innhold">Innhold</label>
          <textarea
            className="form-control"
            id="innhold"
            name="innhold"
            rows="20"
            cols="100"
            value={nyhetStore.currentNyhet.innhold}
            onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
              (nyhetStore.currentNyhet.innhold = event.target.value)
            }>
          </textarea>
        </div>
        <div className="form-group">
          <label for="bildeurl">Bildeurl</label>
          <input
            className="form-control"
            id="bildeurl"
            type="text"
            value={nyhetStore.currentNyhet.bildeurl}
            onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
              (nyhetStore.currentNyhet.bildeurl = event.target.value)
            }
          />
        </div>
        <div className="form-group">
          <label for="bildetekst">Bildetekst</label>
          <input
            className="form-control"
            id="bildetekst"
            type="text"
            value={nyhetStore.currentNyhet.bildetekst}
            onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
              (nyhetStore.currentNyhet.bildetekst = event.target.value)
            }
          />
        </div>
        <div className="form-group">
          <label for="kategori">Kategori</label>
          <select
          className="form-control"
          id="kategori"
          name="kategorinavn"
          onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
            (nyhetStore.currentNyhet.kategori_id = Number.parseInt(event.target.value))
          }>
          <option value={nyhetStore.currentNyhet.kategori_id} selected>{nyhetStore.currentNyhet.kategorinavn}</option>
          <option value={1}>Sport</option>
          <option value={2}>Helse</option>
          <option value={3}>Kjendisnytt</option>
          <option value={4}>Miljø</option>
          <option value={5}>Samfunn</option>
          </select>
        </div>
        <div className="form-group">
          <label for="viktighet">Hvor viktig er artikkelen?</label>
           <small id="viktighetHelp" class="form-text text-muted">(1 for å komme på forsiden, 2 hvis artikkelen er mindre viktig)</small>
          <input
            className="form-control"
            id="viktighet"
            type="number"
            max="2"
            min="1"
            value={nyhetStore.currentNyhet.viktighet}
            onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
              (nyhetStore.currentNyhet.viktighet = Number.parseInt(event.target.value))
            }
          />
        </div>
        <button
        id="lagrebtn"
        type="button"
        className="btn btn-primary"
        onClick={this.save}
        data-toggle="tooltip"
        data-placement="right"
        title="Har du husket å fylle ut alt?">
          Lagre
        </button>
      </CardForm>
    );
  }

  mounted() {
    nyhetStore.getNyhet(this.props.match.params.id).catch((error: Error) => Alert.danger(error.message));
  }

  save() {
    nyhetStore
      .updateNyhet()
      .then(() => history.push('/nyheter/' + nyhetStore.currentNyhet.id))
      .catch((error: Error) => Alert.danger(error.message));
  }
}

class NyhetDelete extends Component <{ match: { params: { id: number } } }> {
  render() {
    return (
      <div className="container-fluid">
        <div className="form-group row">
          <div className="col-sm-4"></div>
            <div className="col-sm-4 card text-center mb-3" id="formEdit">
              <p id="deletetext">Er du sikker på at du ønsker å slette artikkelen?</p>
              <div className="row">
              <div className="col-sm-6">
              <button id="jabtn" type="button" className="btn btn-danger btn-lg" onClick={this.delete} data-toggle="tooltip" data-placement="top" title="Slett permanent">
                Ja
              </button>
              </div>
              <div className="col-sm-6">
              <button id="neibtn" type="button" className="btn btn-primary btn-lg" onClick={this.notdelete} data-toggle="tooltip" data-placement="top" title="Gå tilbake">
                Nei
              </button>
              </div>
              </div>
            </div>
          <div className="col-sm-4"></div>
        </div>
      </div>
    );
  }

  mounted() {
    nyhetStore.getNyhet(this.props.match.params.id).catch((error: Error) => Alert.danger(error.message));
  }

  delete() {
    nyhetStore
      .deleteNyhet()
      .then(() => history.push('/nyheter'))
      .catch((error: Error) => Alert.danger(error.message));
  }

  notdelete() {
    history.push('/nyheter/' + nyhetStore.currentNyhet.id);
  }
}

export class NyhetCreate extends Component {
  render() {
    return (
      <CardForm overskrift='Opprett en ny nyhetsartikkel'>
      <div className="form-group">
        <label for="overskrift">Overskrift</label>
        <input
          className="form-control"
          type="text"
          id="overskrift"
          value={nyhetStore.nyNyhet.overskrift}
          onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
            (nyhetStore.nyNyhet.overskrift = event.target.value)
          }
        />
      </div>
      <div className="form-group">
        <label for="innhold">Innhold</label>
        <textarea
          className="form-control"
          id="innhold"
          name="innhold"
          rows="20"
          cols="100"
          value={nyhetStore.nyNyhet.innhold}
          onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
            (nyhetStore.nyNyhet.innhold = event.target.value)
          }>
        </textarea>
      </div>
      <div className="form-group">
        <label for="bildeurl">Bildeurl</label>
        <input
          className="form-control"
          id="bildeurl"
          type="text"
          value={nyhetStore.nyNyhet.bildeurl}
          onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
            (nyhetStore.nyNyhet.bildeurl = event.target.value)
          }
        />
      </div>
      <div className="form-group">
        <label for="bildetekst">Bildetekst</label>
        <input
          className="form-control"
          id="bildetekst"
          type="text"
          value={nyhetStore.nyNyhet.bildetekst}
          onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
            (nyhetStore.nyNyhet.bildetekst = event.target.value)
          }
        />
      </div>
      <div className="form-group">
        <label for="kategori">Kategori</label>
        <select
        className="form-control"
        id="kategori"
        name="kategorinavn"
        onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
          (nyhetStore.nyNyhet.kategori_id = Number.parseInt(event.target.value))
        }>
        <option value={1} defaultValue>Sport</option>
        <option value={2}>Helse</option>
        <option value={3}>Kjendisnytt</option>
        <option value={4}>Miljø</option>
        <option value={5}>Samfunn</option>
        </select>
      </div>
      <div className="form-group">
        <label for="viktighet">Hvor viktig er artikkelen?</label>
         <small id="viktighetHelp" class="form-text text-muted">(1 for å komme på forsiden, 2 hvis artikkelen er mindre viktig)</small>
        <input
          className="form-control"
          id="viktighet"
          type="number"
          max="2"
          min="1"
          value={nyhetStore.nyNyhet.viktighet}
          onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
            (nyhetStore.nyNyhet.viktighet = Number.parseInt(event.target.value))
          }
        />
      </div>
      <button
      id="lagrebtn"
      type="button"
      className="btn btn-primary"
      onClick={this.save}
      data-toggle="tooltip"
      data-placement="right"
      title="Har du husket å fylle ut alt?">
        Lagre
      </button>
    </CardForm>
    );
  }

  save() {
    nyhetStore
      .createNyhet()
      .then(() => history.push('/nyheter/kategori/' + nyhetStore.currentNyhet.kategorinavn))
      .catch((error: Error) => Alert.danger(error.message));
  }

}



class KategoriList extends Component<{ match: { params: { kategorinavn: string } } }> {
  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-sm-1"></div>
          <div className="col-sm-10">
            <div className="card-columns">
              {nyhetStore.kategorinyheter.map(nyhet => (
                <div key={nyhet.id}>
                {console.log("Currentnyhet id når man velger nyhet i kategori " , JSON.stringify(nyhet))}
                <CardMange
                  idlink={'/nyheter/' + nyhet.id}
                  overskrift={nyhet.overskrift}
                  bildeurl={nyhet.bildeurl}
                  bildeteks={nyhet.bildetekst}
                  tidspunkt={nyhet.tidspunkt}>
                </CardMange>
                </div>
              ))}
            </div>
          </div>
        <div className="col-sm-1"></div>
      </div>
    </div>
    );
  }

  mounted() {
    nyhetStore.getKategoriNyheter(this.props.match.params.kategorinavn).catch((error: Error) => Alert.danger(error.message));
  }
}

const root = document.getElementById('root');
if (root)
  ReactDOM.render(
    <HashRouter>
      <div>
        <Alert />
        <Menu />
        <Livefeed />
        <Route exact path="/" component={Home} />
        <Route exact path="/nyheter" component={NyhetList} />
        <Route exact path="/nyheter/:id" component={NyhetDetails} />
        <Route exact path="/nyheter/:id/edit" component={NyhetEdit} />
        <Route exact path="/nyheter/:id/delete" component={NyhetDelete} />
        <Route exact path="/create" component={NyhetCreate} />
        <Route exact path="/nyheter/kategori/:kategorinavn" component={KategoriList} />
      </div>
    </HashRouter>,
    root
  );
