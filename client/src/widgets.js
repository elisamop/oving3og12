// @flow
/* eslint eqeqeq: "off" */

import * as React from 'react';
import { Component } from 'react-simplified';
import { NavLink } from 'react-router-dom';

/**
 * Renders alert messages using Bootstrap classes.
 */
export class Alert extends Component {
  alerts: { text: React.Node, type: string }[] = [];

  render() {
    return (
      <>
        {this.alerts.map((alert, i) => (
          <div key={i} className={'alert alert-' + alert.type} role="alert">
            {alert.text}
            <button
              className="close"
              onClick={() => {
                this.alerts.splice(i, 1);
              }}
            >
              &times;
            </button>
          </div>
        ))}
      </>
    );
  }

  static success(text: React.Node) {
    // To avoid 'Cannot update during an existing state transition' errors, run after current event through setTimeout
    setTimeout(() => {
      for (let instance of Alert.instances()) instance.alerts.push({ text: text, type: 'success' });
    });
  }

  static info(text: React.Node) {
    // To avoid 'Cannot update during an existing state transition' errors, run after current event through setTimeout
    setTimeout(() => {
      for (let instance of Alert.instances()) instance.alerts.push({ text: text, type: 'info' });
    });
  }

  static warning(text: React.Node) {
    // To avoid 'Cannot update during an existing state transition' errors, run after current event through setTimeout
    setTimeout(() => {
      for (let instance of Alert.instances()) instance.alerts.push({ text: text, type: 'warning' });
    });
  }

  static danger(text: React.Node) {
    // To avoid 'Cannot update during an existing state transition' errors, run after current event through setTimeout
    setTimeout(() => {
      for (let instance of Alert.instances()) instance.alerts.push({ text: text, type: 'danger' });
    });
  }
}


/**
 * Setter navbar på menyen
 */
type PropsNBI = {
  children?: React.Node,
  linker: string
}

type PropsNB = {
  children?: React.Node,
  tittel?: string
}


export class NavBar extends Component<PropsNB>{
  render(){
    return(
      <div className="container-fluid">
        <nav className="navbar navbar-expand-lg fixed-top navbar-dark">
          <a className="navbar-brand navbarlink" href="/">
            {this.props.tittel}
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="flexbox collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              {this.props.children}
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}
export class NavBootItem extends Component<PropsNBI>{
  render(){
    return(
      <li className="nav-item">
      <NavLink className="nav-link" exact to={this.props.linker}>
        {this.props.children}
      </NavLink>
      </li>
    );
  }
}

/**
 * Setter Cards på nyheter
 */
type PropsCardNyhet = {
  bildeurl?: string,
  bildetekst?: string,
  overskrift?: string,
  innhold?: string,
  tidspunkt?: string,
  kategori?: string
}
export class CardEn extends Component<PropsCardNyhet> {
  render(){
    return(
      <div className="card mb-3 text-center">
        <img className="card-img-top" src={this.props.bildeurl} alt={this.props.bildetekst}></img>
        <small>{this.props.bildetekst}</small>
        <div className="card-body">
          <h1 className="card-title">{this.props.overskrift}</h1>
          <p>{this.props.innhold}</p>
        </div>
        <div className="card-footer text-muted">
          <small className="text-muted" id="tidspunktcard">Publisert: {this.props.tidspunkt}</small>
          <small className="text-muted" id="kategoricard">{this.props.kategori}</small>
        </div>
      </div>
    );
  }
}


type PropsCardNyheter = {
  idlink: string,
  bildeurl?: string,
  bildeteks?: string,
  overskrift?: string,
  innhold?: string,
  tidspunkt?: string,
}
export class CardMange extends Component<PropsCardNyheter> {
  render(){
    return(
      <div className="card mb-3 text-center">
        <NavLink exact to={this.props.idlink}>
          <img className="card-img-top" src={this.props.bildeurl} alt={this.props.bildeteks}></img>
        </NavLink>
        <div className="card-body">
          <NavLink exact to={this.props.idlink}>
            <h1 className="card-title">{this.props.overskrift}</h1>
          </NavLink>
        </div>
        <div className="card-footer text-muted">
          <small className="text-muted">Publisert: {this.props.tidspunkt}</small>
        </div>
      </div>
    );
  }
}

type PropsForm = {
  overskrift?: string,
  children?: React.Node,
}
export class CardForm extends Component<PropsForm> {
  render(){
    return (
      <div className="container-fluid">
        <div className="form-group row">
          <div className="col-sm-2"></div>
            <div className="col-sm-8 card mb-3" id="formEdit">
              <form>
                <h1>{this.props.overskrift}</h1>
                {this.props.children}
              </form>
            </div>
          <div className="col-sm-2"></div>
        </div>
      </div>
    );
  }
}
