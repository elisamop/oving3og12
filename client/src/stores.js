// @flow
import { sharedComponentData } from 'react-simplified';
import axios from 'axios';
axios.interceptors.response.use(response => response.data);

export class Nyhet {
  id: number = 0;
  overskrift: string = '';
  innhold: string = '';
  tidspunkt: string = '';
  bildeurl: string = '';
  bildetekst: string = '';
  kategori_id: number = 1;
  viktighet: number = 1;
  kategorinavn: string = '';
}

export class NyhetStore {
  nyheter : Nyhet[] = [];
  livefeed : Nyhet[] = [];
  kategorinyheter : Nyhet[] = [];
  currentNyhet = new Nyhet();
  nyNyhet = new Nyhet();

  //Henter alle nyheter//
  getNyheter() {
    console.log("henter alle nyheter");
    return axios.get('/nyheter').then((nyheter : Nyhet[]) => (this.nyheter = nyheter));
  }

  //Henter alle nyheter på kategoriId//
  getKategoriNyheter(kategorinavn: string) {
    console.log("henter nyheter til valgt kategori");
    this.kategorinyheter = [];
    return axios.get('/nyheter/kategori/' + kategorinavn).then((kategorinyheter: Nyhet[]) => (this.kategorinyheter = kategorinyheter));
  }

  //Henter livefeed nyheter//
  getLivefeedNyheter() {
    console.log("Henter livefeeden");
    return axios.get('/nyheter/livefeed').then((livefeed: Nyhet[]) => (this.livefeed = livefeed));
  }

  //Henter én bestemt nyhet på id//
  getNyhet(id: number) {
    console.log("henter valgt nyhet");
    return axios.get('/nyheter/' + id).then((nyhet: (Nyhet) => mixed) => {
      // Oppdaterer nyhetsarrayet i tilfelle informasjonen har endret seg
      for (let e of this.nyheter) {
        console.log("kjører for-løkke i getNyhet()");
        console.log("forsøker å skrive ut nyhet: " ,nyhet[0]);
        if (e.id == nyhet[0].id) {
          e.overskrift = nyhet[0].overskrift;
          e.innhold = nyhet[0].innhold;
          e.tidspunkt = nyhet[0].tidspunkt;
          e.bildeurl = nyhet[0].bildeurl;
          e.bildetekst = nyhet[0].bildetekst;
          e.kategori_id = nyhet[0].kategori_id;
          e.viktighet = nyhet[0].viktighet;
          break;
        }
      }
      //Setter current nyhet lik nyheten vi henter fra databasen
      this.currentNyhet = nyhet[0];
      //Setter kategorinavn til current nyhet
      switch(nyhet[0].kategori_id){
        case 1:
          this.currentNyhet.kategorinavn = "Sport";
          break;
        case 2:
          this.currentNyhet.kategorinavn = "Helse";
          break;
        case 3:
          this.currentNyhet.kategorinavn = "Kjendisnytt";
          break;
        case 4:
          this.currentNyhet.kategorinavn = "Miljø";
          break;
        case 5:
          this.currentNyhet.kategorinavn = "Samfunn";
          break;
        default:
        break;
      }
      console.log("kategorinavn fra current " + this.currentNyhet.kategorinavn);
    });
  }

  //Oppdaterer en nyhet//
  updateNyhet() {
    console.log("Oppdaterer nyhet " + this.currentNyhet.id);
    return axios.put('/nyheter/' + this.currentNyhet.id, this.currentNyhet).then(() => {
      // Oppdaterer nyhetsarrayet
      for (let e of this.nyheter) {
        if (e.id == this.currentNyhet.id) {
          e.overskrift = this.currentNyhet.overskrift;
          e.innhold = this.currentNyhet.innhold;
          e.bildeurl = this.currentNyhet.bildeurl;
          e.bildetekst = this.currentNyhet.bildetekst;
          e.kategori_id = this.currentNyhet.kategori_id;
          e.viktighet = this.currentNyhet.viktighet;
          break;
        }
      }
    });
  }

  //Sletter en nyhet//
  deleteNyhet(){
    console.log("Sletter nyhet " + this.currentNyhet.id);
    return axios.delete('/nyheter/' + this.currentNyhet.id);
  }

  //Oppretter en nyhet//
  createNyhet(){
    console.log("Oppretter en ny nyhet");
    return axios.post('/nyheter', this.nyNyhet).then(() => {
      //For å finne kategorinavn når man skal bytte over til kategorisiden etter at artikkelen er lagt ut
      switch(this.nyNyhet.kategori_id){
          case 1:
            this.nyNyhet.kategorinavn = "Sport";
            break;
          case 2:
            this.nyNyhet.kategorinavn = "Helse";
            break;
          case 3:
            this.nyNyhet.kategorinavn = "Kjendisnytt";
            break;
          case 4:
            this.nyNyhet.kategorinavn = "Miljø";
            break;
          case 5:
            this.nyNyhet.kategorinavn = "Samfunn";
            break;
          default:
          break;
        }

        this.currentNyhet = this.nyNyhet;
        this.nyNyhet = new Nyhet();
    });
  }




}

export let nyhetStore = sharedComponentData(new NyhetStore());
