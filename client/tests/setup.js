// @flow

import React from 'react';
import Enzyme, { configure, render, mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });
