// @flow

import * as React from 'react';
import ReactDOM from 'react-dom';
import { Component } from 'react-simplified';
import { Alert, NavBar, NavBootItem, CardEn, CardMange, CardForm } from '../src/widgets.js';
import { shallow, mount } from 'enzyme';
import { nyhetStore } from '../src/stores';
import {NyhetEdit, NyhetCreate} from '../src/index.js';

describe('Rendering tests', () => {

  it('renders NavBar without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<NavBar />, div);
    ReactDOM.unmountComponentAtNode(div);  //return true hvis componenten er unmounted, false hvis det ikke er noe komponent å unmounte
  });
  it('renders NavBootItem without crashing', () => {
    const wrapper = shallow(<NavBootItem linker="/nyheter" />);
    expect(wrapper).toMatchSnapshot();
    });
  it('renders CardEn without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<CardEn />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
  it('renders CardEn image without crashing', () => {
    const img = document.createElement('img');
    ReactDOM.render(<CardEn />, img);
    ReactDOM.unmountComponentAtNode(img);
  });
  it('renders CardMange without crashing', () => {
    const wrapper = shallow(<CardMange idlink="/nyheter/1" />);
    expect(wrapper).toMatchSnapshot();
  });
  it('renders CardForm without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<CardForm />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

});

describe('Components props', () => {
  it('NavBar Props', () => {
    const navbar = shallow(<NavBar tittel="nyheter" />);
    expect(navbar.find('a').text()).toEqual("nyheter");
  });
  it('<NavBootItem /> Props', () => {
    const navbootitem = shallow(<NavBootItem linker="/nyheter/kategori/Sport" />);
    expect(navbootitem.find('NavLink').prop('to')).toEqual("/nyheter/kategori/Sport");
  });
  it('<CardMange /> Props', () => {
    const nyheter = shallow(<CardMange idlink="/nyheter/1" bildeurl="www.google.no"/>);
    expect(nyheter.find('h1').length).toEqual(1);
    expect(nyheter.find('NavLink').first().prop('to')).toEqual('/nyheter/1');
    expect(nyheter.find('NavLink').at(1).prop('to')).toEqual('/nyheter/1');
    expect(nyheter.find('img').prop('src')).toEqual("www.google.no");
  });
  it('<CardEn /> Props', () => {
    const nyhet = shallow(<CardEn bildeurl="www.google.no" bildetekst="googlebildeteksttull" tidspunkt='2018-10-22 kl 16:13:22' />);
    expect(nyhet.find('img').prop('src')).toEqual("www.google.no");
    expect(nyhet.find('img').prop('alt')).toEqual("googlebildeteksttull");
    expect(nyhet.find('small').first().text()).toEqual("googlebildeteksttull");
    expect(nyhet.find('small').at(1).text()).toEqual("Publisert: 2018-10-22 kl 16:13:22");
  });
  //Disse testene passer uansett av en ellerannen grunn
  it('<CardForm /> Props', () => {
    nyhetStore.currentNyhet.overskrift = "Hei";
    const formet = shallow(<CardForm overskrift="Edit nyhet" />);
    expect(formet.find('h1').first().text()).toEqual("Edit nyhet");
    //expect(formet.find('label').first().text()).toEqual('Innhold');
    //expect(formet.find('input').first().prop('value')).toEqual("Hallo");
  });

});

// describe('<CardForm /> ', () => {
//   const wrapper = mount(<CardForm />);
//   it('Initially', () => {
//     let instance: ?CardForm = CardForm.instance();
//     expect(typeof instance).toEqual('object');
//
//     expect(wrapper.find('#overskrift').toHaveLength(1));
//
//
//   });
//   it('writing', done => {
//
//     wrapper.find('#overskrift').simulate('onChange', {
//       target: {value: 'Hallo'}
//     });
//
//     setTimeout(() => {
//       expect(wrapper.find('#overskrift').toHaveLength(1));
//
//       expect(wrapper.find('#overskrift').prop('value')).toEqual('Hallo');
//
//       done();
//       });
//
//   });
// });

describe('Alert tests', () => {
  const wrapper = shallow(<Alert />);

  it('initially', () => {
    let instance: ?Alert = Alert.instance();
    expect(typeof instance).toEqual('object');
    if (instance) expect(instance.alerts).toEqual([]);

    expect(wrapper.find('button.close')).toHaveLength(0);
  });

  it('after danger', done => {
    Alert.danger('test');

    setTimeout(() => {
      let instance: ?Alert = Alert.instance();
      expect(typeof instance).toEqual('object');
      if (instance) expect(instance.alerts).toEqual([{ text: 'test', type: 'danger' }]);

      expect(wrapper.find('button.close')).toHaveLength(1);

      done();
    });
  });

  it('after clicking close button', () => {
    wrapper.find('button.close').simulate('click');

    let instance: ?Alert = Alert.instance();
    expect(typeof instance).toEqual('object');
    if (instance) expect(instance.alerts).toEqual([]);

    expect(wrapper.find('button.close')).toHaveLength(0);
  });
});
